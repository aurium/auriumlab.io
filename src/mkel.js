function camelToKebab(camelCase) {
    return camelCase.replace(/([A-Z])/g, (m,l)=> '-'+l.toLocaleLowerCase());
}

function parseTagExt(tagExt) {
    tagExt = tagExt.replace(/([$_])/g, ' $1\0').split(' ').map(att=>att.split('\0'));
    let id, className = '', tag = tagExt.shift()[0];
    if (!tag) tag = 'div';
    tagExt.forEach(att => {
        if (att[0] === '_') className += ' ' + camelToKebab(att[1]);
        else id = camelToKebab(att[1]);
    });
    if (!className) className = null;
    return { tag, id, className };
}

function appendChildren(el, children) {
    if (children.forEach)
        children.forEach(([tagExt, attrs])=>
            mkEl(tagExt, {...attrs, parent:el})
        );
    else
        Object.keys(children).forEach(tagExt => {
            let attrs = children[tagExt];
            if (typeof(attrs) === 'string') attrs = { text: attrs };
            el.mkChild(tagExt, attrs)
        });
}

function mkEl(tagExt, attrs={}) {
    if (typeof(attrs) === 'string') attrs = { text: attrs };
    let {tag, id, className} = parseTagExt(tagExt);
    attrs.id = id;
    attrs.class = attrs.class ? attrs.class + ' ' + className : className;
    let el = extendElement(document.createElement(tag));
    Object.keys(attrs).forEach((att)=> {
        let val = attrs[att];
        if (typeof(val) !== 'undefined' && val !== null) {
            att == 'text'
            ? el.appendChild(document.createTextNode(val))
            : att == 'html'
            ? el.innerHTML = val
            : att == 'parent'
            ? val.appendChild(el)
            : att == 'children'
            ? appendChildren(el, val)
            : att.match(/^on/)
            ? el[att] = val
            : el.setAttribute(att, val)
        }
    });
    return el;
}

function extendElement(el) {
    let mkChild = (tag, attrs)=> {
        if (typeof(attrs) === 'string') attrs = { text: attrs };
        return mkEl(tag, {...attrs, parent: el});
    }
    el.mkChild = new Proxy(mkChild, mkElHandler);
    return el;
}

const mkElHandler = {
    get(func, tag) {
        return (attrs)=> func(tag, attrs);
    }
};

export default new Proxy(mkEl, mkElHandler);
export { extendElement };
