import mkEl, { extendElement } from './mkel.js?t=%BUILD_DATE%';

"use strict";

const activityListEl = extendElement(document.getElementById('activity'));

function clearMD(code) {
    return code
    .replace(/[*`]/g, '')
    .replace(/([\[( "'.,;:!?])_/g, '$1')
    .replace(/_([\]) "'.,;:!?])/g, '$1')
}

function normaliseGithubEvent(evOrig) {
    var ev = { ...evOrig, repo: {...evOrig.repo} };
    ev.repo.name = evOrig.repo.name.replace(/.*\//, '');
    switch (evOrig.type) {
        case 'PullRequestEvent':
            ev.evName = 'Merge Request';
            ev.intro = evOrig.payload.pull_request.title;
            ev.url = evOrig.payload.pull_request.html_url;
            break;

        case 'PushEvent':
            ev.evName = 'Push';
            ev.intro = clearMD(evOrig.payload.commits[0].message);
            //ev.url = evOrig.payload.commits[0].url;
            ev.url = `https://github.com/${evOrig.repo.name}/commit/${ev.payload.head}`;
            break;

        case 'IssueCommentEvent':
        case 'PullRequestReviewCommentEvent':
            ev.evName = 'Comment';
            ev.intro = clearMD(evOrig.payload.comment.body);
            ev.url = evOrig.payload.comment.html_url;
            break;
    }
    return ev;
}

async function normaliseGitlabEvent(evOrig) {
    var ev = {
        ...evOrig,
        type: evOrig.action_name + (evOrig.target_type ? '-'+evOrig.target_type : ''),
        repo: await getGitlabProject(evOrig.project_id)
    };
    ev.created_at = ev.created_at.replace(/\.[0-9]+Z/, 'Z');
    if (!ev.repo) return {};
    if (!evOrig.action_name) console.log('IGNORE', ev);
    ev.repo.url = ev.repo.web_url;
    switch (ev.type) {
        case 'pushed to':
            console.log(ev)
            ev.evName = 'Push';
            ev.intro = evOrig.push_data.commit_title;
            ev.url = ev.repo.url + '/-/commit/' + ev.push_data.commit_to;
            if (/Merge branch .* into/.test(ev.intro)) return {};
            break;
        case 'accepted-MergeRequest':
            ev.evName = 'Accept Merge Request';
            ev.intro = evOrig.target_title;
            ev.url = ev.repo.url + '/-/merge_requests/' + ev.target_iid;
            break;
    }
    return ev;
}

var gitlabProjects = {};
async function getGitlabProject(id) {
    if (!gitlabProjects[id]) {
        console.log('REQ', id);
        gitlabProjects[id] =
        fetch(`https://gitlab.com/api/v4/projects/${id}?statistics=true`)
        .then(response => response.json())
        .then(project => gitlabProjects[id] = project)
        .catch(err => {
            delete gitlabProjects[id];
            console.error('Fail while loading a project.', id, err);
            return null;
        });
    }
    return gitlabProjects[id];
}

async function processActivityEvents(gitlabEvs, githubEvs) {
    // ignore this site events.
    gitlabEvs = gitlabEvs.filter(ev => ev.project_id !== 2253465);
    return [
        ...(await Promise.all(gitlabEvs.map(normaliseGitlabEvent))),
        ...githubEvs.map(normaliseGithubEvent)
    ]
    .filter(ev => {
        if (ev.evName) return true;
        else console.log(`Ignore git manager event: "${ev.type}".`);
    })
    .sort((ev1, ev2)=> ev1.created_at > ev2.created_at ? -1 : 1)
}

async function displayActivityEvents(gitlabEvs, githubEvs) {
    (await processActivityEvents(gitlabEvs, githubEvs))
    .forEach(ev =>
        activityListEl.mkChild.li({
            children: {
                a_action: {
                    href: ev.url,
                    children: {
                        span_evName: ev.evName,
                        span: ' ',
                        span_evIntro: `"${ev.intro}"`
                    }
                },
                span: ' at ',
                a_repo: { href: ev.repo.url, text: ev.repo.name }
            }
        })
    )
    activityListEl.classList.remove('loading');
}

var gitlabEvs = [{
    "project_id": 2253465,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-16T19:38:54.814Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 1,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "ef6c75726f0162d624d8c219415ef7ac5c327272",
        "commit_to": "3b477b806eed75d37f2b1eb3a0a4af1de70ad7fe",
        "ref": "master",
        "commit_title": "Filter projects and add some from Github.",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 2253465,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-15T23:57:27.724Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 1,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "5ab1df035082234ae8f709f106aa3faaaa97185e",
        "commit_to": "ef6c75726f0162d624d8c219415ef7ac5c327272",
        "ref": "master",
        "commit_title": "Enforce loading new build and some async care.",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 2253465,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-15T17:50:04.881Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 1,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "0c11977d117886c370c871d76ada4476d267c7ae",
        "commit_to": "5ab1df035082234ae8f709f106aa3faaaa97185e",
        "ref": "master",
        "commit_title": "Migrasting to SASS",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 2253465,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-15T16:35:20.781Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 1,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "a2d51e558650e87e0f2321f68ab12109a40f488f",
        "commit_to": "0c11977d117886c370c871d76ada4476d267c7ae",
        "ref": "master",
        "commit_title": "Move public to src, the begning of buildable...",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 2253465,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-15T14:20:23.041Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 2,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "fd2a3e2669fa6e59209aa3ce5188a9d72c4d7d24",
        "commit_to": "a2d51e558650e87e0f2321f68ab12109a40f488f",
        "ref": "master",
        "commit_title": "Add color mark to langs",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 2253465,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-14T22:55:36.626Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 1,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "9fd791e86ca7185428abca12e7653105bca1f600",
        "commit_to": "fd2a3e2669fa6e59209aa3ce5188a9d72c4d7d24",
        "ref": "master",
        "commit_title": "Copy aurium.one and add projects from GitLab's API",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "pushed to",
    "target_id": null,
    "target_iid": null,
    "target_type": null,
    "author_id": 2773,
    "target_title": null,
    "created_at": "2020-05-13T19:06:59.947Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "push_data": {
        "commit_count": 6,
        "action": "pushed",
        "ref_type": "branch",
        "commit_from": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
        "commit_to": "ee4b70ac7e67d644a43254afc9cd950fe0ee6e34",
        "ref": "master",
        "commit_title": "Merge branch 'feature-03' into 'master'",
        "ref_count": null
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "accepted",
    "target_id": 52104483,
    "target_iid": 13,
    "target_type": "MergeRequest",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T19:06:59.944Z",
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341997356,
    "target_iid": 341997356,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:17:18.690Z",
    "note": {
        "id": 341997356,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:17:18.482Z",
        "updated_at": "2020-05-13T18:45:22.857Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/update-route-handler.ts",
            "new_path": "handlers/update-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 91,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341997183,
    "target_iid": 341997183,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:17:00.119Z",
    "note": {
        "id": 341997183,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:16:59.753Z",
        "updated_at": "2020-05-13T18:45:20.907Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/update-route-handler.ts",
            "new_path": "handlers/update-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 85,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341996882,
    "target_iid": 341996882,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:16:21.446Z",
    "note": {
        "id": 341996882,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:16:21.158Z",
        "updated_at": "2020-05-13T18:45:18.796Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/update-route-handler.ts",
            "new_path": "handlers/update-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 71,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341995845,
    "target_iid": 341995845,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:14:04.154Z",
    "note": {
        "id": 341995845,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:14:03.861Z",
        "updated_at": "2020-05-13T18:45:16.144Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/update-route-handler.ts",
            "new_path": "handlers/update-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 64,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341995659,
    "target_iid": 341995659,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:13:38.621Z",
    "note": {
        "id": 341995659,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:13:38.390Z",
        "updated_at": "2020-05-13T18:45:13.406Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/route-getter-handler.ts",
            "new_path": "handlers/route-getter-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 76,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341995219,
    "target_iid": 341995219,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:12:45.855Z",
    "note": {
        "id": 341995219,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:12:45.592Z",
        "updated_at": "2020-05-13T18:45:11.391Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/route-getter-handler.ts",
            "new_path": "handlers/route-getter-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 64,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341994876,
    "target_iid": 341994876,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:12:00.996Z",
    "note": {
        "id": 341994876,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:12:00.649Z",
        "updated_at": "2020-05-13T18:45:08.246Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/history-getter-handler.ts",
            "new_path": "handlers/history-getter-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 85,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341994804,
    "target_iid": 341994804,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:11:49.497Z",
    "note": {
        "id": 341994804,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:11:49.026Z",
        "updated_at": "2020-05-13T18:45:04.239Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/history-getter-handler.ts",
            "new_path": "handlers/history-getter-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 72,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341994536,
    "target_iid": 341994536,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:11:18.123Z",
    "note": {
        "id": 341994536,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:11:17.842Z",
        "updated_at": "2020-05-13T18:45:01.362Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/delete-ws-route-handler.ts",
            "new_path": "handlers/delete-ws-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 51,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341994340,
    "target_iid": 341994340,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:10:55.698Z",
    "note": {
        "id": 341994340,
        "type": "DiffNote",
        "body": ":+1:",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:10:55.413Z",
        "updated_at": "2020-05-13T18:44:58.358Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/delete-ws-route-handler.ts",
            "new_path": "handlers/delete-ws-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 45,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341993612,
    "target_iid": 341993612,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T18:09:17.254Z",
    "note": {
        "id": 341993612,
        "type": "DiffNote",
        "body": "Esse 204 me incomoda um pouco. O objeto jsend não tem dados ou mensagem, mas ainda existe conteúdo na resposta do serv. Bem, fica aí pra reflexão. Se você concordar que vale a pena mudar, deixe para outro MR.",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T18:09:17.010Z",
        "updated_at": "2020-05-13T18:44:55.570Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "6dc2c6c0e88403b89e49340570865f9087fe229b",
            "old_path": "handlers/clear-history-handler.ts",
            "new_path": "handlers/clear-history-handler.ts",
            "position_type": "text",
            "old_line": 45,
            "new_line": 43,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}, {
    "project_id": 15447484,
    "action_name": "commented on",
    "target_id": 341983414,
    "target_iid": 341983414,
    "target_type": "DiffNote",
    "author_id": 2773,
    "target_title": "feature-03 - Allow to update a route",
    "created_at": "2020-05-13T17:47:47.072Z",
    "note": {
        "id": 341983414,
        "type": "DiffNote",
        "body": "idem",
        "attachment": null,
        "author": {
            "id": 2773,
            "name": "Aurélio A. Heckert",
            "username": "aurium",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
            "web_url": "https://gitlab.com/aurium"
        },
        "created_at": "2020-05-13T17:47:46.509Z",
        "updated_at": "2020-05-13T18:44:53.591Z",
        "system": false,
        "noteable_id": 52104483,
        "noteable_type": "MergeRequest",
        "position": {
            "base_sha": "752c8735efa7e1fff21ac3aea5874a59d5561387",
            "start_sha": "9c8a7fd77e360c5c98bb7f24f12ca08676ee6531",
            "head_sha": "933925af0a2701cc149fd7cd4af32af379c34262",
            "old_path": "handlers/delete-route-handler.ts",
            "new_path": "handlers/delete-route-handler.ts",
            "position_type": "text",
            "old_line": null,
            "new_line": 52,
            "line_range": null
        },
        "resolvable": true,
        "resolved": true,
        "resolved_by": {
            "id": 4953822,
            "name": "Iury Dias",
            "username": "iiurydias",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4953822/avatar.png",
            "web_url": "https://gitlab.com/iiurydias"
        },
        "confidential": null,
        "noteable_iid": 13,
        "commands_changes": {}
    },
    "author": {
        "id": 2773,
        "name": "Aurélio A. Heckert",
        "username": "aurium",
        "state": "active",
        "avatar_url": "https://secure.gravatar.com/avatar/24a625896a07aa37fdb2352e302e96de?s=80&d=identicon",
        "web_url": "https://gitlab.com/aurium"
    },
    "author_username": "aurium"
}];

var githubEvs = [
  {
    "id": "12312676423",
    "type": "PullRequestEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 48750889,
      "name": "sdumetz/node-xdg-parse",
      "url": "https://api.github.com/repos/sdumetz/node-xdg-parse"
    },
    "payload": {
      "action": "opened",
      "number": 12,
      "pull_request": {
        "url": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/12",
        "id": 416755563,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE2NzU1NTYz",
        "html_url": "https://github.com/sdumetz/node-xdg-parse/pull/12",
        "diff_url": "https://github.com/sdumetz/node-xdg-parse/pull/12.diff",
        "patch_url": "https://github.com/sdumetz/node-xdg-parse/pull/12.patch",
        "issue_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues/12",
        "number": 12,
        "state": "open",
        "locked": false,
        "title": "Always return array for known plural keys.",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "To simplify normalization and usage of the parsed entry result.",
        "created_at": "2020-05-12T14:15:50Z",
        "updated_at": "2020-05-12T14:15:51Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": null,
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/12/commits",
        "review_comments_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/12/comments",
        "review_comment_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues/12/comments",
        "statuses_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/statuses/d4cb4bf3b39cc9c8b3701d050ebc33e4c969b05e",
        "head": {
          "label": "aurium:master",
          "ref": "master",
          "sha": "d4cb4bf3b39cc9c8b3701d050ebc33e4c969b05e",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 258847254,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNTg4NDcyNTQ=",
            "name": "node-xdg-parse",
            "full_name": "aurium/node-xdg-parse",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/node-xdg-parse",
            "description": "XDG configuration files syntax parser",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/node-xdg-parse",
            "forks_url": "https://api.github.com/repos/aurium/node-xdg-parse/forks",
            "keys_url": "https://api.github.com/repos/aurium/node-xdg-parse/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/node-xdg-parse/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/node-xdg-parse/teams",
            "hooks_url": "https://api.github.com/repos/aurium/node-xdg-parse/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/node-xdg-parse/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/node-xdg-parse/events",
            "assignees_url": "https://api.github.com/repos/aurium/node-xdg-parse/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/node-xdg-parse/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/node-xdg-parse/tags",
            "blobs_url": "https://api.github.com/repos/aurium/node-xdg-parse/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/node-xdg-parse/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/node-xdg-parse/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/node-xdg-parse/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/node-xdg-parse/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/node-xdg-parse/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/node-xdg-parse/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/node-xdg-parse/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/node-xdg-parse/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/node-xdg-parse/subscription",
            "commits_url": "https://api.github.com/repos/aurium/node-xdg-parse/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/node-xdg-parse/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/node-xdg-parse/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/node-xdg-parse/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/node-xdg-parse/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/node-xdg-parse/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/node-xdg-parse/merges",
            "archive_url": "https://api.github.com/repos/aurium/node-xdg-parse/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/node-xdg-parse/downloads",
            "issues_url": "https://api.github.com/repos/aurium/node-xdg-parse/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/node-xdg-parse/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/node-xdg-parse/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/node-xdg-parse/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/node-xdg-parse/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/node-xdg-parse/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/node-xdg-parse/deployments",
            "created_at": "2020-04-25T18:35:26Z",
            "updated_at": "2020-05-12T14:11:27Z",
            "pushed_at": "2020-05-12T14:11:25Z",
            "git_url": "git://github.com/aurium/node-xdg-parse.git",
            "ssh_url": "git@github.com:aurium/node-xdg-parse.git",
            "clone_url": "https://github.com/aurium/node-xdg-parse.git",
            "svn_url": "https://github.com/aurium/node-xdg-parse",
            "homepage": null,
            "size": 89,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "JavaScript",
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "sdumetz:master",
          "ref": "master",
          "sha": "01c023b4c1e5bb2b4dfbb8d6c1d64bf35e68b847",
          "user": {
            "login": "sdumetz",
            "id": 4112885,
            "node_id": "MDQ6VXNlcjQxMTI4ODU=",
            "avatar_url": "https://avatars3.githubusercontent.com/u/4112885?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/sdumetz",
            "html_url": "https://github.com/sdumetz",
            "followers_url": "https://api.github.com/users/sdumetz/followers",
            "following_url": "https://api.github.com/users/sdumetz/following{/other_user}",
            "gists_url": "https://api.github.com/users/sdumetz/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/sdumetz/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/sdumetz/subscriptions",
            "organizations_url": "https://api.github.com/users/sdumetz/orgs",
            "repos_url": "https://api.github.com/users/sdumetz/repos",
            "events_url": "https://api.github.com/users/sdumetz/events{/privacy}",
            "received_events_url": "https://api.github.com/users/sdumetz/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 48750889,
            "node_id": "MDEwOlJlcG9zaXRvcnk0ODc1MDg4OQ==",
            "name": "node-xdg-parse",
            "full_name": "sdumetz/node-xdg-parse",
            "private": false,
            "owner": {
              "login": "sdumetz",
              "id": 4112885,
              "node_id": "MDQ6VXNlcjQxMTI4ODU=",
              "avatar_url": "https://avatars3.githubusercontent.com/u/4112885?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/sdumetz",
              "html_url": "https://github.com/sdumetz",
              "followers_url": "https://api.github.com/users/sdumetz/followers",
              "following_url": "https://api.github.com/users/sdumetz/following{/other_user}",
              "gists_url": "https://api.github.com/users/sdumetz/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/sdumetz/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/sdumetz/subscriptions",
              "organizations_url": "https://api.github.com/users/sdumetz/orgs",
              "repos_url": "https://api.github.com/users/sdumetz/repos",
              "events_url": "https://api.github.com/users/sdumetz/events{/privacy}",
              "received_events_url": "https://api.github.com/users/sdumetz/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/sdumetz/node-xdg-parse",
            "description": "XDG configuration files syntax parser",
            "fork": false,
            "url": "https://api.github.com/repos/sdumetz/node-xdg-parse",
            "forks_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/forks",
            "keys_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/teams",
            "hooks_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/hooks",
            "issue_events_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues/events{/number}",
            "events_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/events",
            "assignees_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/assignees{/user}",
            "branches_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/branches{/branch}",
            "tags_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/tags",
            "blobs_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/languages",
            "stargazers_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/stargazers",
            "contributors_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/contributors",
            "subscribers_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/subscribers",
            "subscription_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/subscription",
            "commits_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/contents/{+path}",
            "compare_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/merges",
            "archive_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/downloads",
            "issues_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues{/number}",
            "pulls_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/labels{/name}",
            "releases_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/releases{/id}",
            "deployments_url": "https://api.github.com/repos/sdumetz/node-xdg-parse/deployments",
            "created_at": "2015-12-29T14:23:05Z",
            "updated_at": "2020-04-29T06:36:40Z",
            "pushed_at": "2020-04-29T06:36:37Z",
            "git_url": "git://github.com/sdumetz/node-xdg-parse.git",
            "ssh_url": "git@github.com:sdumetz/node-xdg-parse.git",
            "clone_url": "https://github.com/sdumetz/node-xdg-parse.git",
            "svn_url": "https://github.com/sdumetz/node-xdg-parse",
            "homepage": null,
            "size": 84,
            "stargazers_count": 2,
            "watchers_count": 2,
            "language": "JavaScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 2,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 1,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 2,
            "open_issues": 1,
            "watchers": 2,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/12"
          },
          "html": {
            "href": "https://github.com/sdumetz/node-xdg-parse/pull/12"
          },
          "issue": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues/12"
          },
          "comments": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/issues/12/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/12/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/pulls/12/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/sdumetz/node-xdg-parse/statuses/d4cb4bf3b39cc9c8b3701d050ebc33e4c969b05e"
          }
        },
        "author_association": "CONTRIBUTOR",
        "merged": false,
        "mergeable": null,
        "rebaseable": null,
        "mergeable_state": "unknown",
        "merged_by": null,
        "comments": 0,
        "review_comments": 0,
        "maintainer_can_modify": true,
        "commits": 1,
        "additions": 36,
        "deletions": 3,
        "changed_files": 3
      }
    },
    "public": true,
    "created_at": "2020-05-12T14:15:51Z"
  },
  {
    "id": "12312622205",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 258847254,
      "name": "aurium/node-xdg-parse",
      "url": "https://api.github.com/repos/aurium/node-xdg-parse"
    },
    "payload": {
      "push_id": 5059936406,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "d4cb4bf3b39cc9c8b3701d050ebc33e4c969b05e",
      "before": "01c023b4c1e5bb2b4dfbb8d6c1d64bf35e68b847",
      "commits": [
        {
          "sha": "d4cb4bf3b39cc9c8b3701d050ebc33e4c969b05e",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Always return array for known plural keys.",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/d4cb4bf3b39cc9c8b3701d050ebc33e4c969b05e"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-12T14:11:26Z"
  },
  {
    "id": "12306693969",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 258847254,
      "name": "aurium/node-xdg-parse",
      "url": "https://api.github.com/repos/aurium/node-xdg-parse"
    },
    "payload": {
      "push_id": 5056826812,
      "size": 11,
      "distinct_size": 5,
      "ref": "refs/heads/master",
      "head": "01c023b4c1e5bb2b4dfbb8d6c1d64bf35e68b847",
      "before": "9a8a92cf31471448b409d7268c4c453c2554b9db",
      "commits": [
        {
          "sha": "0df8be0918cf0be8a24b6ec0ec0360bc5cc35f86",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "set-up github actions CI",
          "distinct": false,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/0df8be0918cf0be8a24b6ec0ec0360bc5cc35f86"
        },
        {
          "sha": "87210425a072b4d1816ad819fdc6607c7197625c",
          "author": {
            "email": "30254+aurium@users.noreply.github.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Support special locations\n\nAdd support to `@` location modifier and hyphened locations.\r\n\r\ncloses #3\r\ncloses #6",
          "distinct": false,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/87210425a072b4d1816ad819fdc6607c7197625c"
        },
        {
          "sha": "9907be6252f20a40fbddb4af83685035a95284fe",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "simplify testing with snapshot test",
          "distinct": false,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/9907be6252f20a40fbddb4af83685035a95284fe"
        },
        {
          "sha": "ca58c128be5cde08f1d228da5d04bfde86a1086f",
          "author": {
            "email": "30254+aurium@users.noreply.github.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Adds support to empty values\n\ncloses #7",
          "distinct": false,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/ca58c128be5cde08f1d228da5d04bfde86a1086f"
        },
        {
          "sha": "2f8de0fbe607360e8e6de05660b3c9733bd305c9",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "add a test for empty string values and interpolate number and boolean values",
          "distinct": false,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/2f8de0fbe607360e8e6de05660b3c9733bd305c9"
        },
        {
          "sha": "5e5d46bd46dd5d9325647f14eb3e2d725961763b",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "add tests for partial locale support",
          "distinct": false,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/5e5d46bd46dd5d9325647f14eb3e2d725961763b"
        },
        {
          "sha": "52bffdf09087884da9f0ba5287653e3492da4f12",
          "author": {
            "email": "30254+aurium@users.noreply.github.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Auto split array values (#11)\n\nThe `splitArrayValue()` separates the feature, not only for legibility, but also to allow forcing some key to always to be an array.",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/52bffdf09087884da9f0ba5287653e3492da4f12"
        },
        {
          "sha": "7048a4fcbf1e9301ffa1bdb8f246ede8b3b2952c",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "update tests to match auto-split array value",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/7048a4fcbf1e9301ffa1bdb8f246ede8b3b2952c"
        },
        {
          "sha": "ee55dc46329ed3b7c207319ccbbdd5bd300a6d26",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "1.0.0",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/ee55dc46329ed3b7c207319ccbbdd5bd300a6d26"
        },
        {
          "sha": "dd61081408523cacb3a1aa5d4bf3ea424667bcfe",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "update dev ependencies (no module changes)",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/dd61081408523cacb3a1aa5d4bf3ea424667bcfe"
        },
        {
          "sha": "01c023b4c1e5bb2b4dfbb8d6c1d64bf35e68b847",
          "author": {
            "email": "s.dumetz@holusion.com",
            "name": "Sebastien DUMETZ"
          },
          "message": "rename github workflow",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/node-xdg-parse/commits/01c023b4c1e5bb2b4dfbb8d6c1d64bf35e68b847"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-12T02:15:18Z"
  },
  {
    "id": "12303827873",
    "type": "IssueCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261910710,
      "name": "GeoffreyBooth/node-loaders",
      "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders"
    },
    "payload": {
      "action": "created",
      "issue": {
        "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1",
        "repository_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders",
        "labels_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1/labels{/name}",
        "comments_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1/comments",
        "events_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1/events",
        "html_url": "https://github.com/GeoffreyBooth/node-loaders/issues/1",
        "id": 615946299,
        "node_id": "MDU6SXNzdWU2MTU5NDYyOTk=",
        "number": 1,
        "title": "Discussion: How to sum N loaders?",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "closed",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 1,
        "created_at": "2020-05-11T14:50:32Z",
        "updated_at": "2020-05-11T19:23:41Z",
        "closed_at": "2020-05-11T15:31:55Z",
        "author_association": "COLLABORATOR",
        "body": "I believe `pgp-loader` alone scarcely will be an use case. Cryptography makes sense if the data must be stored or traffic by insecure places. In a real case it could work together with `https-loader` or (pending)`imap-loader`. Like that, many use cases may need to sum loaders to achieve the desired feature.\r\n\r\nI think \"`--experimental-loader`\" don't support an array of loaders, so what is the best path? Implement the support on Node.js or make a loader of loaders? _(I believe it must be a Node.js feature)_\r\n\r\nRegardless of the chosen path, simply create a line of hooks (where the \"defaultHook\" parameter is the next loader hook) is enough? I think yes. We must to consider something?"
      },
      "comment": {
        "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/comments/626907096",
        "html_url": "https://github.com/GeoffreyBooth/node-loaders/issues/1#issuecomment-626907096",
        "issue_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1",
        "id": 626907096,
        "node_id": "MDEyOklzc3VlQ29tbWVudDYyNjkwNzA5Ng==",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "created_at": "2020-05-11T19:23:41Z",
        "updated_at": "2020-05-11T19:23:41Z",
        "author_association": "COLLABORATOR",
        "body": "Moved to https://github.com/nodejs/modules/issues/513"
      }
    },
    "public": true,
    "created_at": "2020-05-11T19:23:41Z"
  },
  {
    "id": "12303817609",
    "type": "IssuesEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 119901830,
      "name": "nodejs/modules",
      "url": "https://api.github.com/repos/nodejs/modules"
    },
    "payload": {
      "action": "opened",
      "issue": {
        "url": "https://api.github.com/repos/nodejs/modules/issues/513",
        "repository_url": "https://api.github.com/repos/nodejs/modules",
        "labels_url": "https://api.github.com/repos/nodejs/modules/issues/513/labels{/name}",
        "comments_url": "https://api.github.com/repos/nodejs/modules/issues/513/comments",
        "events_url": "https://api.github.com/repos/nodejs/modules/issues/513/events",
        "html_url": "https://github.com/nodejs/modules/issues/513",
        "id": 616122636,
        "node_id": "MDU6SXNzdWU2MTYxMjI2MzY=",
        "number": 513,
        "title": "Using multiple loaders",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 0,
        "created_at": "2020-05-11T19:22:34Z",
        "updated_at": "2020-05-11T19:22:34Z",
        "closed_at": null,
        "author_association": "NONE",
        "body": "Imagine having transpiler loaders for N languages, network access loaders, encrypted source loaders... To some tasks we may need more than one loader in a Node.js project.\r\n\r\nI think \"`--experimental-loader`\" don't support an array of loaders, so what is the best path? Implement the support on Node.js or make a loader of loaders? _(I believe it must be a Node.js feature)_\r\n\r\nRegardless of the chosen path, simply create a line of hooks (where the \"defaultHook\" parameter is the next loader hook) is enough? I think yes. Is there something to consider?"
      }
    },
    "public": true,
    "created_at": "2020-05-11T19:22:35Z",
    "org": {
      "id": 9950313,
      "login": "nodejs",
      "gravatar_id": "",
      "url": "https://api.github.com/orgs/nodejs",
      "avatar_url": "https://avatars.githubusercontent.com/u/9950313?"
    }
  },
  {
    "id": "12302513090",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261903793,
      "name": "aurium/coffeescript",
      "url": "https://api.github.com/repos/aurium/coffeescript"
    },
    "payload": {
      "push_id": 5054638794,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "2dba49d02b444a80b80f1c8abf08a156398a9d95",
      "before": "3ff4583c4183f5ca22cc30b67f356fc499f89509",
      "commits": [
        {
          "sha": "2dba49d02b444a80b80f1c8abf08a156398a9d95",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Move `import.meta`'s error testing to the correct place",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript/commits/2dba49d02b444a80b80f1c8abf08a156398a9d95"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T17:06:08Z"
  },
  {
    "id": "12302483906",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261903793,
      "name": "aurium/coffeescript",
      "url": "https://api.github.com/repos/aurium/coffeescript"
    },
    "payload": {
      "push_id": 5054623900,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "3ff4583c4183f5ca22cc30b67f356fc499f89509",
      "before": "9b770237a3492235b6f5251c0899fc00d083d7f4",
      "commits": [
        {
          "sha": "3ff4583c4183f5ca22cc30b67f356fc499f89509",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Move 's error testing to the correct place",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript/commits/3ff4583c4183f5ca22cc30b67f356fc499f89509"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T17:03:13Z"
  },
  {
    "id": "12302481971",
    "type": "PullRequestReviewCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "created",
      "comment": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments/423186134",
        "pull_request_review_id": 409360247,
        "id": 423186134,
        "node_id": "MDI0OlB1bGxSZXF1ZXN0UmV2aWV3Q29tbWVudDQyMzE4NjEzNA==",
        "diff_hunk": "@@ -941,3 +941,57 @@ test \"#4834: dynamic import\", ->\n       return bar = (await import('bar'));\n     };\n   \"\"\"\n+\n+test \"#5317: Support import.meta\", ->\n+  eqJS \"\"\"\n+    foo = import.meta\n+  \"\"\",\n+  \"\"\"\n+    var foo;\n+\n+    foo = import.meta;\n+  \"\"\"\n+\n+  eqJS \"\"\"\n+    foo = import\n+        .meta\n+  \"\"\",\n+  \"\"\"\n+    var foo;\n+\n+    foo = import.meta;\n+  \"\"\"\n+\n+  eqJS \"\"\"\n+    foo = import.\n+        meta\n+  \"\"\",\n+  \"\"\"\n+    var foo;\n+\n+    foo = import.meta;\n+  \"\"\"\n+\n+  eqJS \"\"\"\n+    foo = import.meta.bar\n+  \"\"\",\n+  \"\"\"\n+    var foo;\n+\n+    foo = import.meta.bar;\n+  \"\"\"\n+\n+  eqJS \"\"\"\n+    foo = import\n+        .meta\n+        .bar\n+  \"\"\",\n+  \"\"\"\n+    var foo;\n+\n+    foo = import.meta.bar;\n+  \"\"\"\n+\n+  throwsCompileError \"\"\"",
        "path": "test/modules.coffee",
        "position": 55,
        "original_position": 55,
        "commit_id": "9b770237a3492235b6f5251c0899fc00d083d7f4",
        "original_commit_id": "d13c15bc99968030bce03fdadb63406d72ea902a",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Done. Thanks!",
        "created_at": "2020-05-11T17:03:00Z",
        "updated_at": "2020-05-11T17:03:01Z",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319#discussion_r423186134",
        "pull_request_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319",
        "author_association": "NONE",
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments/423186134"
          },
          "html": {
            "href": "https://github.com/jashkenas/coffeescript/pull/5319#discussion_r423186134"
          },
          "pull_request": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319"
          }
        },
        "start_line": null,
        "original_start_line": null,
        "start_side": null,
        "line": 995,
        "original_line": 995,
        "side": "RIGHT",
        "in_reply_to_id": 423027302
      },
      "pull_request": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319",
        "id": 415767768,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NzY3NzY4",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319",
        "diff_url": "https://github.com/jashkenas/coffeescript/pull/5319.diff",
        "patch_url": "https://github.com/jashkenas/coffeescript/pull/5319.patch",
        "issue_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319",
        "number": 5319,
        "state": "open",
        "locked": false,
        "title": "Support import.meta and import.meta.*",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Allows to access `import.meta` and only if its property is `meta`.\r\n\r\nThe new token `IMPORT_META` was created to allow richer formatting and code analysis.\r\n```\r\n$ ./bin/coffee --tokens -e 'import.meta.something'\r\n[IMPORT_META import] [. .] [PROPERTY meta] [. .] [PROPERTY something] [TERMINATOR \\n]\r\n```\r\ncloses #5317",
        "created_at": "2020-05-10T19:20:08Z",
        "updated_at": "2020-05-11T17:03:01Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": "c091de6979526639685d4b3c92de806419eb8d4d",
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/commits",
        "review_comments_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/comments",
        "review_comment_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments",
        "statuses_url": "https://api.github.com/repos/jashkenas/coffeescript/statuses/9b770237a3492235b6f5251c0899fc00d083d7f4",
        "head": {
          "label": "aurium:master",
          "ref": "master",
          "sha": "9b770237a3492235b6f5251c0899fc00d083d7f4",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 261903793,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNjE5MDM3OTM=",
            "name": "coffeescript",
            "full_name": "aurium/coffeescript",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/coffeescript",
            "description": "Unfancy JavaScript",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/coffeescript",
            "forks_url": "https://api.github.com/repos/aurium/coffeescript/forks",
            "keys_url": "https://api.github.com/repos/aurium/coffeescript/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/coffeescript/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/coffeescript/teams",
            "hooks_url": "https://api.github.com/repos/aurium/coffeescript/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/coffeescript/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/coffeescript/events",
            "assignees_url": "https://api.github.com/repos/aurium/coffeescript/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/coffeescript/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/coffeescript/tags",
            "blobs_url": "https://api.github.com/repos/aurium/coffeescript/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/coffeescript/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/coffeescript/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/coffeescript/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/coffeescript/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/coffeescript/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/coffeescript/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/coffeescript/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/coffeescript/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/coffeescript/subscription",
            "commits_url": "https://api.github.com/repos/aurium/coffeescript/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/coffeescript/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/coffeescript/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/coffeescript/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/coffeescript/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/coffeescript/merges",
            "archive_url": "https://api.github.com/repos/aurium/coffeescript/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/coffeescript/downloads",
            "issues_url": "https://api.github.com/repos/aurium/coffeescript/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/coffeescript/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/coffeescript/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/coffeescript/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/coffeescript/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/coffeescript/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/coffeescript/deployments",
            "created_at": "2020-05-06T23:40:26Z",
            "updated_at": "2020-05-11T16:54:14Z",
            "pushed_at": "2020-05-11T16:54:11Z",
            "git_url": "git://github.com/aurium/coffeescript.git",
            "ssh_url": "git@github.com:aurium/coffeescript.git",
            "clone_url": "https://github.com/aurium/coffeescript.git",
            "svn_url": "https://github.com/aurium/coffeescript",
            "homepage": "https://coffeescript.org/",
            "size": 29357,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "CoffeeScript",
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "jashkenas:master",
          "ref": "master",
          "sha": "75d376f2eff7108ea8bb91d4326aac62989c8556",
          "user": {
            "login": "jashkenas",
            "id": 4732,
            "node_id": "MDQ6VXNlcjQ3MzI=",
            "avatar_url": "https://avatars3.githubusercontent.com/u/4732?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/jashkenas",
            "html_url": "https://github.com/jashkenas",
            "followers_url": "https://api.github.com/users/jashkenas/followers",
            "following_url": "https://api.github.com/users/jashkenas/following{/other_user}",
            "gists_url": "https://api.github.com/users/jashkenas/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/jashkenas/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/jashkenas/subscriptions",
            "organizations_url": "https://api.github.com/users/jashkenas/orgs",
            "repos_url": "https://api.github.com/users/jashkenas/repos",
            "events_url": "https://api.github.com/users/jashkenas/events{/privacy}",
            "received_events_url": "https://api.github.com/users/jashkenas/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 441269,
            "node_id": "MDEwOlJlcG9zaXRvcnk0NDEyNjk=",
            "name": "coffeescript",
            "full_name": "jashkenas/coffeescript",
            "private": false,
            "owner": {
              "login": "jashkenas",
              "id": 4732,
              "node_id": "MDQ6VXNlcjQ3MzI=",
              "avatar_url": "https://avatars3.githubusercontent.com/u/4732?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/jashkenas",
              "html_url": "https://github.com/jashkenas",
              "followers_url": "https://api.github.com/users/jashkenas/followers",
              "following_url": "https://api.github.com/users/jashkenas/following{/other_user}",
              "gists_url": "https://api.github.com/users/jashkenas/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/jashkenas/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/jashkenas/subscriptions",
              "organizations_url": "https://api.github.com/users/jashkenas/orgs",
              "repos_url": "https://api.github.com/users/jashkenas/repos",
              "events_url": "https://api.github.com/users/jashkenas/events{/privacy}",
              "received_events_url": "https://api.github.com/users/jashkenas/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/jashkenas/coffeescript",
            "description": "Unfancy JavaScript",
            "fork": false,
            "url": "https://api.github.com/repos/jashkenas/coffeescript",
            "forks_url": "https://api.github.com/repos/jashkenas/coffeescript/forks",
            "keys_url": "https://api.github.com/repos/jashkenas/coffeescript/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/jashkenas/coffeescript/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/jashkenas/coffeescript/teams",
            "hooks_url": "https://api.github.com/repos/jashkenas/coffeescript/hooks",
            "issue_events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/events{/number}",
            "events_url": "https://api.github.com/repos/jashkenas/coffeescript/events",
            "assignees_url": "https://api.github.com/repos/jashkenas/coffeescript/assignees{/user}",
            "branches_url": "https://api.github.com/repos/jashkenas/coffeescript/branches{/branch}",
            "tags_url": "https://api.github.com/repos/jashkenas/coffeescript/tags",
            "blobs_url": "https://api.github.com/repos/jashkenas/coffeescript/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/jashkenas/coffeescript/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/jashkenas/coffeescript/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/jashkenas/coffeescript/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/jashkenas/coffeescript/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/jashkenas/coffeescript/languages",
            "stargazers_url": "https://api.github.com/repos/jashkenas/coffeescript/stargazers",
            "contributors_url": "https://api.github.com/repos/jashkenas/coffeescript/contributors",
            "subscribers_url": "https://api.github.com/repos/jashkenas/coffeescript/subscribers",
            "subscription_url": "https://api.github.com/repos/jashkenas/coffeescript/subscription",
            "commits_url": "https://api.github.com/repos/jashkenas/coffeescript/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/jashkenas/coffeescript/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/jashkenas/coffeescript/contents/{+path}",
            "compare_url": "https://api.github.com/repos/jashkenas/coffeescript/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/jashkenas/coffeescript/merges",
            "archive_url": "https://api.github.com/repos/jashkenas/coffeescript/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/jashkenas/coffeescript/downloads",
            "issues_url": "https://api.github.com/repos/jashkenas/coffeescript/issues{/number}",
            "pulls_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/jashkenas/coffeescript/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/jashkenas/coffeescript/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/labels{/name}",
            "releases_url": "https://api.github.com/repos/jashkenas/coffeescript/releases{/id}",
            "deployments_url": "https://api.github.com/repos/jashkenas/coffeescript/deployments",
            "created_at": "2009-12-18T01:39:53Z",
            "updated_at": "2020-05-11T11:54:28Z",
            "pushed_at": "2020-05-11T16:54:13Z",
            "git_url": "git://github.com/jashkenas/coffeescript.git",
            "ssh_url": "git@github.com:jashkenas/coffeescript.git",
            "clone_url": "https://github.com/jashkenas/coffeescript.git",
            "svn_url": "https://github.com/jashkenas/coffeescript",
            "homepage": "https://coffeescript.org/",
            "size": 29192,
            "stargazers_count": 15535,
            "watchers_count": 15535,
            "language": "CoffeeScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": true,
            "forks_count": 2005,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 47,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 2005,
            "open_issues": 47,
            "watchers": 15535,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319"
          },
          "html": {
            "href": "https://github.com/jashkenas/coffeescript/pull/5319"
          },
          "issue": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319"
          },
          "comments": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/statuses/9b770237a3492235b6f5251c0899fc00d083d7f4"
          }
        },
        "author_association": "NONE"
      }
    },
    "public": true,
    "created_at": "2020-05-11T17:03:00Z"
  },
  {
    "id": "12302405840",
    "type": "PullRequestReviewCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "created",
      "comment": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments/423181212",
        "pull_request_review_id": 409354254,
        "id": 423181212,
        "node_id": "MDI0OlB1bGxSZXF1ZXN0UmV2aWV3Q29tbWVudDQyMzE4MTIxMg==",
        "diff_hunk": "@@ -186,7 +186,10 @@ exports.Lexer = class Lexer\n       else if tag is 'UNLESS'\n         tag = 'IF'\n       else if tag is 'IMPORT'\n-        @seenImport = yes\n+        if match.input.match /^import\\s*\\.\\s*[_a-zA-Z$][_a-zA-Z$0-9]*/",
        "path": "src/lexer.coffee",
        "position": null,
        "original_position": 5,
        "commit_id": "9b770237a3492235b6f5251c0899fc00d083d7f4",
        "original_commit_id": "d13c15bc99968030bce03fdadb63406d72ea902a",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Done!",
        "created_at": "2020-05-11T16:55:24Z",
        "updated_at": "2020-05-11T16:55:24Z",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319#discussion_r423181212",
        "pull_request_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319",
        "author_association": "NONE",
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments/423181212"
          },
          "html": {
            "href": "https://github.com/jashkenas/coffeescript/pull/5319#discussion_r423181212"
          },
          "pull_request": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319"
          }
        },
        "start_line": null,
        "original_start_line": null,
        "start_side": null,
        "line": null,
        "original_line": 189,
        "side": "RIGHT",
        "in_reply_to_id": 423024516
      },
      "pull_request": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319",
        "id": 415767768,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NzY3NzY4",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319",
        "diff_url": "https://github.com/jashkenas/coffeescript/pull/5319.diff",
        "patch_url": "https://github.com/jashkenas/coffeescript/pull/5319.patch",
        "issue_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319",
        "number": 5319,
        "state": "open",
        "locked": false,
        "title": "Support import.meta and import.meta.*",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Allows to access `import.meta` and only if its property is `meta`.\r\n\r\nThe new token `IMPORT_META` was created to allow richer formatting and code analysis.\r\n```\r\n$ ./bin/coffee --tokens -e 'import.meta.something'\r\n[IMPORT_META import] [. .] [PROPERTY meta] [. .] [PROPERTY something] [TERMINATOR \\n]\r\n```\r\ncloses #5317",
        "created_at": "2020-05-10T19:20:08Z",
        "updated_at": "2020-05-11T16:55:24Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": "c091de6979526639685d4b3c92de806419eb8d4d",
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/commits",
        "review_comments_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/comments",
        "review_comment_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments",
        "statuses_url": "https://api.github.com/repos/jashkenas/coffeescript/statuses/9b770237a3492235b6f5251c0899fc00d083d7f4",
        "head": {
          "label": "aurium:master",
          "ref": "master",
          "sha": "9b770237a3492235b6f5251c0899fc00d083d7f4",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 261903793,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNjE5MDM3OTM=",
            "name": "coffeescript",
            "full_name": "aurium/coffeescript",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/coffeescript",
            "description": "Unfancy JavaScript",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/coffeescript",
            "forks_url": "https://api.github.com/repos/aurium/coffeescript/forks",
            "keys_url": "https://api.github.com/repos/aurium/coffeescript/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/coffeescript/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/coffeescript/teams",
            "hooks_url": "https://api.github.com/repos/aurium/coffeescript/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/coffeescript/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/coffeescript/events",
            "assignees_url": "https://api.github.com/repos/aurium/coffeescript/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/coffeescript/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/coffeescript/tags",
            "blobs_url": "https://api.github.com/repos/aurium/coffeescript/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/coffeescript/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/coffeescript/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/coffeescript/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/coffeescript/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/coffeescript/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/coffeescript/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/coffeescript/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/coffeescript/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/coffeescript/subscription",
            "commits_url": "https://api.github.com/repos/aurium/coffeescript/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/coffeescript/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/coffeescript/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/coffeescript/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/coffeescript/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/coffeescript/merges",
            "archive_url": "https://api.github.com/repos/aurium/coffeescript/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/coffeescript/downloads",
            "issues_url": "https://api.github.com/repos/aurium/coffeescript/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/coffeescript/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/coffeescript/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/coffeescript/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/coffeescript/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/coffeescript/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/coffeescript/deployments",
            "created_at": "2020-05-06T23:40:26Z",
            "updated_at": "2020-05-11T16:54:14Z",
            "pushed_at": "2020-05-11T16:54:11Z",
            "git_url": "git://github.com/aurium/coffeescript.git",
            "ssh_url": "git@github.com:aurium/coffeescript.git",
            "clone_url": "https://github.com/aurium/coffeescript.git",
            "svn_url": "https://github.com/aurium/coffeescript",
            "homepage": "https://coffeescript.org/",
            "size": 29357,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "CoffeeScript",
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "jashkenas:master",
          "ref": "master",
          "sha": "75d376f2eff7108ea8bb91d4326aac62989c8556",
          "user": {
            "login": "jashkenas",
            "id": 4732,
            "node_id": "MDQ6VXNlcjQ3MzI=",
            "avatar_url": "https://avatars3.githubusercontent.com/u/4732?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/jashkenas",
            "html_url": "https://github.com/jashkenas",
            "followers_url": "https://api.github.com/users/jashkenas/followers",
            "following_url": "https://api.github.com/users/jashkenas/following{/other_user}",
            "gists_url": "https://api.github.com/users/jashkenas/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/jashkenas/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/jashkenas/subscriptions",
            "organizations_url": "https://api.github.com/users/jashkenas/orgs",
            "repos_url": "https://api.github.com/users/jashkenas/repos",
            "events_url": "https://api.github.com/users/jashkenas/events{/privacy}",
            "received_events_url": "https://api.github.com/users/jashkenas/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 441269,
            "node_id": "MDEwOlJlcG9zaXRvcnk0NDEyNjk=",
            "name": "coffeescript",
            "full_name": "jashkenas/coffeescript",
            "private": false,
            "owner": {
              "login": "jashkenas",
              "id": 4732,
              "node_id": "MDQ6VXNlcjQ3MzI=",
              "avatar_url": "https://avatars3.githubusercontent.com/u/4732?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/jashkenas",
              "html_url": "https://github.com/jashkenas",
              "followers_url": "https://api.github.com/users/jashkenas/followers",
              "following_url": "https://api.github.com/users/jashkenas/following{/other_user}",
              "gists_url": "https://api.github.com/users/jashkenas/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/jashkenas/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/jashkenas/subscriptions",
              "organizations_url": "https://api.github.com/users/jashkenas/orgs",
              "repos_url": "https://api.github.com/users/jashkenas/repos",
              "events_url": "https://api.github.com/users/jashkenas/events{/privacy}",
              "received_events_url": "https://api.github.com/users/jashkenas/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/jashkenas/coffeescript",
            "description": "Unfancy JavaScript",
            "fork": false,
            "url": "https://api.github.com/repos/jashkenas/coffeescript",
            "forks_url": "https://api.github.com/repos/jashkenas/coffeescript/forks",
            "keys_url": "https://api.github.com/repos/jashkenas/coffeescript/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/jashkenas/coffeescript/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/jashkenas/coffeescript/teams",
            "hooks_url": "https://api.github.com/repos/jashkenas/coffeescript/hooks",
            "issue_events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/events{/number}",
            "events_url": "https://api.github.com/repos/jashkenas/coffeescript/events",
            "assignees_url": "https://api.github.com/repos/jashkenas/coffeescript/assignees{/user}",
            "branches_url": "https://api.github.com/repos/jashkenas/coffeescript/branches{/branch}",
            "tags_url": "https://api.github.com/repos/jashkenas/coffeescript/tags",
            "blobs_url": "https://api.github.com/repos/jashkenas/coffeescript/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/jashkenas/coffeescript/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/jashkenas/coffeescript/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/jashkenas/coffeescript/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/jashkenas/coffeescript/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/jashkenas/coffeescript/languages",
            "stargazers_url": "https://api.github.com/repos/jashkenas/coffeescript/stargazers",
            "contributors_url": "https://api.github.com/repos/jashkenas/coffeescript/contributors",
            "subscribers_url": "https://api.github.com/repos/jashkenas/coffeescript/subscribers",
            "subscription_url": "https://api.github.com/repos/jashkenas/coffeescript/subscription",
            "commits_url": "https://api.github.com/repos/jashkenas/coffeescript/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/jashkenas/coffeescript/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/jashkenas/coffeescript/contents/{+path}",
            "compare_url": "https://api.github.com/repos/jashkenas/coffeescript/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/jashkenas/coffeescript/merges",
            "archive_url": "https://api.github.com/repos/jashkenas/coffeescript/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/jashkenas/coffeescript/downloads",
            "issues_url": "https://api.github.com/repos/jashkenas/coffeescript/issues{/number}",
            "pulls_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/jashkenas/coffeescript/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/jashkenas/coffeescript/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/labels{/name}",
            "releases_url": "https://api.github.com/repos/jashkenas/coffeescript/releases{/id}",
            "deployments_url": "https://api.github.com/repos/jashkenas/coffeescript/deployments",
            "created_at": "2009-12-18T01:39:53Z",
            "updated_at": "2020-05-11T11:54:28Z",
            "pushed_at": "2020-05-11T16:54:13Z",
            "git_url": "git://github.com/jashkenas/coffeescript.git",
            "ssh_url": "git@github.com:jashkenas/coffeescript.git",
            "clone_url": "https://github.com/jashkenas/coffeescript.git",
            "svn_url": "https://github.com/jashkenas/coffeescript",
            "homepage": "https://coffeescript.org/",
            "size": 29192,
            "stargazers_count": 15535,
            "watchers_count": 15535,
            "language": "CoffeeScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": true,
            "forks_count": 2005,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 47,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 2005,
            "open_issues": 47,
            "watchers": 15535,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319"
          },
          "html": {
            "href": "https://github.com/jashkenas/coffeescript/pull/5319"
          },
          "issue": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319"
          },
          "comments": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/statuses/9b770237a3492235b6f5251c0899fc00d083d7f4"
          }
        },
        "author_association": "NONE"
      }
    },
    "public": true,
    "created_at": "2020-05-11T16:55:24Z"
  },
  {
    "id": "12302393376",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261903793,
      "name": "aurium/coffeescript",
      "url": "https://api.github.com/repos/aurium/coffeescript"
    },
    "payload": {
      "push_id": 5054577861,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "9b770237a3492235b6f5251c0899fc00d083d7f4",
      "before": "5e0953ee3bb879a8f83b40763e635efd4ec24492",
      "commits": [
        {
          "sha": "9b770237a3492235b6f5251c0899fc00d083d7f4",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Do not \"lex ahead\" `import.meta`",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript/commits/9b770237a3492235b6f5251c0899fc00d083d7f4"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T16:54:12Z"
  },
  {
    "id": "12302279026",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261903793,
      "name": "aurium/coffeescript",
      "url": "https://api.github.com/repos/aurium/coffeescript"
    },
    "payload": {
      "push_id": 5054518925,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "5e0953ee3bb879a8f83b40763e635efd4ec24492",
      "before": "d13c15bc99968030bce03fdadb63406d72ea902a",
      "commits": [
        {
          "sha": "5e0953ee3bb879a8f83b40763e635efd4ec24492",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Add AST tests for import.meta",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript/commits/5e0953ee3bb879a8f83b40763e635efd4ec24492"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T16:42:42Z"
  },
  {
    "id": "12301035151",
    "type": "IssuesEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261910710,
      "name": "GeoffreyBooth/node-loaders",
      "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders"
    },
    "payload": {
      "action": "opened",
      "issue": {
        "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1",
        "repository_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders",
        "labels_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1/labels{/name}",
        "comments_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1/comments",
        "events_url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/issues/1/events",
        "html_url": "https://github.com/GeoffreyBooth/node-loaders/issues/1",
        "id": 615946299,
        "node_id": "MDU6SXNzdWU2MTU5NDYyOTk=",
        "number": 1,
        "title": "Discussion: How to sum N loaders?",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 0,
        "created_at": "2020-05-11T14:50:32Z",
        "updated_at": "2020-05-11T14:50:32Z",
        "closed_at": null,
        "author_association": "COLLABORATOR",
        "body": "I believe `pgp-loader` alone scarcely will be an use case. Cryptography makes sense if the data must be stored or traffic by insecure places. In a real case it could work together with `https-loader` or (pending)`imap-loader`. Like that, many use cases may need to sum loaders to achieve the desired feature.\r\n\r\nI think \"`--experimental-loader`\" don't support an array of loaders, so what is the best path? Implement the support on Node.js or make a loader of loaders? _(I believe it must be a Node.js feature)_\r\n\r\nRegardless of the chosen path, simply create a line of hooks (where the \"defaultHook\" parameter is the next loader hook) is enough? I think yes. We must to consider something?"
      }
    },
    "public": true,
    "created_at": "2020-05-11T14:50:32Z"
  },
  {
    "id": "12300733363",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261910710,
      "name": "GeoffreyBooth/node-loaders",
      "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders"
    },
    "payload": {
      "push_id": 5053717002,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "05017190746bef0776d175310b1c38fc3c270e13",
      "before": "f78ab68467aaede6c5ee6a1e90842c14a082fd34",
      "commits": [
        {
          "sha": "05017190746bef0776d175310b1c38fc3c270e13",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Finish usable PGP Loader",
          "distinct": true,
          "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/commits/05017190746bef0776d175310b1c38fc3c270e13"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T14:24:50Z"
  },
  {
    "id": "12295249756",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261910710,
      "name": "GeoffreyBooth/node-loaders",
      "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders"
    },
    "payload": {
      "push_id": 5050861771,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "f78ab68467aaede6c5ee6a1e90842c14a082fd34",
      "before": "7f85962d3b6f96ced38854b57d722092c43c7877",
      "commits": [
        {
          "sha": "f78ab68467aaede6c5ee6a1e90842c14a082fd34",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "WIP: Adds PGP Loader",
          "distinct": true,
          "url": "https://api.github.com/repos/GeoffreyBooth/node-loaders/commits/f78ab68467aaede6c5ee6a1e90842c14a082fd34"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T03:17:05Z"
  },
  {
    "id": "12294579386",
    "type": "IssueCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "created",
      "issue": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319",
        "repository_url": "https://api.github.com/repos/jashkenas/coffeescript",
        "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/labels{/name}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments",
        "events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/events",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319",
        "id": 615457909,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NzY3NzY4",
        "number": 5319,
        "title": "Support import.meta and import.meta.*",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 1,
        "created_at": "2020-05-10T19:20:08Z",
        "updated_at": "2020-05-11T00:37:38Z",
        "closed_at": null,
        "author_association": "NONE",
        "pull_request": {
          "url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319",
          "html_url": "https://github.com/jashkenas/coffeescript/pull/5319",
          "diff_url": "https://github.com/jashkenas/coffeescript/pull/5319.diff",
          "patch_url": "https://github.com/jashkenas/coffeescript/pull/5319.patch"
        },
        "body": "Allows to access `import.meta` and only if its property is `meta`.\r\n\r\nThe new token `IMPORT_META` was created to allow richer formatting and code analysis.\r\n```\r\n$ ./bin/coffee --tokens -e 'import.meta.something'\r\n[IMPORT_META import] [. .] [PROPERTY meta] [. .] [PROPERTY something] [TERMINATOR \\n]\r\n```\r\ncloses #5317"
      },
      "comment": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/comments/626415830",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319#issuecomment-626415830",
        "issue_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319",
        "id": 626415830,
        "node_id": "MDEyOklzc3VlQ29tbWVudDYyNjQxNTgzMA==",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "created_at": "2020-05-11T00:37:38Z",
        "updated_at": "2020-05-11T00:37:38Z",
        "author_association": "NONE",
        "body": "@vendethiel, now it can. Thanks!"
      }
    },
    "public": true,
    "created_at": "2020-05-11T00:37:38Z"
  },
  {
    "id": "12294555830",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261903793,
      "name": "aurium/coffeescript",
      "url": "https://api.github.com/repos/aurium/coffeescript"
    },
    "payload": {
      "push_id": 5050470347,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "d13c15bc99968030bce03fdadb63406d72ea902a",
      "before": "ebba62c2a3c4fac2ddaeaadac0cb2649a065628b",
      "commits": [
        {
          "sha": "d13c15bc99968030bce03fdadb63406d72ea902a",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Allow white spacing arround `import.meta` property accessor",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript/commits/d13c15bc99968030bce03fdadb63406d72ea902a"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-11T00:30:56Z"
  },
  {
    "id": "12294401444",
    "type": "IssueCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "created",
      "issue": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320",
        "repository_url": "https://api.github.com/repos/jashkenas/coffeescript",
        "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320/labels{/name}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320/comments",
        "events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320/events",
        "html_url": "https://github.com/jashkenas/coffeescript/issues/5320",
        "id": 615487193,
        "node_id": "MDU6SXNzdWU2MTU0ODcxOTM=",
        "number": 5320,
        "title": "Discussion: Barista, the Pluggable CoffeeScript",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 2,
        "created_at": "2020-05-10T22:09:33Z",
        "updated_at": "2020-05-10T23:44:24Z",
        "closed_at": null,
        "author_association": "NONE",
        "body": "## Barista, who can build new flavors of coffee for you.\r\n\r\nIt looks like we agree that is interesting to have a pluggable coffee compiler in the \"[TypeScript Output](https://github.com/jashkenas/coffeescript/issues/5307)\" discussion, for a health evolution of the language and the CoffeeScript ecosystem.\r\n\r\nAs I understand, the CoffeeScript source is a elaborated [Jison](http://zaa.ch/jison) configuration, and the compiler itself is a parser code generated by Jison. Make this generated code pluggable can be hard to do and harder to maintain.\r\n\r\nI believe, the right path to make a pluggable coffee compiler is to have `jison` as a execution dependency and the compiler itself must to be build at runtime.\r\n\r\nWell it obviously will make the coffee compiler heavier and slower.\r\n\r\nSo I propose a new build task on the Cakefile to create the **\"barista compiler\"**. It will be the same CoffeeScript compiler source, but not generated by Jison, its API will have plugin related configs, as its bin's CLI will have some plugins args. And, sure, it must be shipped with a different `package.json` with `\"jison\"` in the `\"dependencies\"` list.\r\n\r\n## How a plugin may work\r\n\r\nA plugin must implement methods to parameterize, extend or replace the transpiler units:\r\n  * `Lexer.tokenize()`: Looks like it will be the harder process to become pluggable. The method itself may stay untouchable by plugins, but they must to be able to change or parameterize its specialized tokenizers (`identifierToken()`, `stringToken()`, ...) or add new ones on any position of the [tokenizers call line](https://github.com/jashkenas/coffeescript/blob/75d376f2eff7108ea8bb91d4326aac62989c8556/src/lexer.coffee#L69-L80).\r\n  * `grammar` must be exported as it is and freely modifiable by the plugin code.\r\n  * `nodes` module looks like the most important is already exported and can be changed by the plugin. But it need to be clear where a new collection of node classes must to be attached to be used by the extended transpiler. Some `nodes.coffee` helpers must to be exported to become usable by new node classes, and it must export a method to enable the plugins to replace this helpers if needed.\r\n\r\n```\r\n┌────────────────────────────────────────────────────────┐\r\n│ barista + plugin1 + plugin2 + user-src.flavored-coffee │\r\n└────────────────────────────────────────────────────────┘\r\n                          ⇩\r\n              ╔════════════════════════╗\r\n              ║    Barista Compiler    ║\r\n              ╟────────────────────────╢\r\n              ║  ┌──────────────────┐  ║\r\n              ║  │ CoffeeScript Src ├┐ ║\r\n              ║  └┬─────────────────┘│ ║\r\n              ║   └──────────────────┘ ║\r\n              ║           ⇩            ║\r\n              ║  ┏━━━━━━━━━━━━━━━━━━┓  ║\r\n              ║  ┃     Plugin 1     ┃  ║\r\n              ║  ┗━━━━━━━━━━━━━━━━━━┛  ║\r\n              ║           ⇩            ║\r\n              ║  ┏━━━━━━━━━━━━━━━━━━┓  ║\r\n              ║  ┃     Plugin 2     ┃  ║\r\n              ║  ┗━━━━━━━━━━━━━━━━━━┛  ║\r\n              ║           ⇩            ║\r\n              ║  ╔══════════════════╗  ║\r\n              ║  ║  Cache Flavored  ║  ║\r\n              ║  ║   CoffeeScript   ║  ║\r\n              ║  ║    Transpiler    ║  ║\r\n              ║  ╚══════════════════╝  ║\r\n              ╚════════════════════════╝\r\n                          ⇩\r\n              ┌───────────────────────┐\r\n              │  user-src.transpiled  │\r\n              └───────────────────────┘\r\n```\r\n\r\nWhat you think? What is missed? Is there a better path? Do you know specific details that need to be described?"
      },
      "comment": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/comments/626408120",
        "html_url": "https://github.com/jashkenas/coffeescript/issues/5320#issuecomment-626408120",
        "issue_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320",
        "id": 626408120,
        "node_id": "MDEyOklzc3VlQ29tbWVudDYyNjQwODEyMA==",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "created_at": "2020-05-10T23:44:24Z",
        "updated_at": "2020-05-10T23:44:24Z",
        "author_association": "NONE",
        "body": "Wow! Let me get my leaking noob brain back.\r\n\r\nJison looks like a good tool to me, however makes CoffeeScript static looks better. After my first contribution to the compiler i wold like to have something more solid in my hands. How hard will be to accomplish this decoupling?\r\n\r\nI agree that lex/grammar functions should be sync, however limit pre and post processing to be sync may be very bad for many plugins. As this feature can push a major release, I believe it will be ok to break compatibility with CoffeeScript module users."
      }
    },
    "public": true,
    "created_at": "2020-05-10T23:44:25Z"
  },
  {
    "id": "12294272193",
    "type": "IssuesEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 2079310,
      "name": "disnet/contracts.coffee",
      "url": "https://api.github.com/repos/disnet/contracts.coffee"
    },
    "payload": {
      "action": "opened",
      "issue": {
        "url": "https://api.github.com/repos/disnet/contracts.coffee/issues/65",
        "repository_url": "https://api.github.com/repos/disnet/contracts.coffee",
        "labels_url": "https://api.github.com/repos/disnet/contracts.coffee/issues/65/labels{/name}",
        "comments_url": "https://api.github.com/repos/disnet/contracts.coffee/issues/65/comments",
        "events_url": "https://api.github.com/repos/disnet/contracts.coffee/issues/65/events",
        "html_url": "https://github.com/disnet/contracts.coffee/issues/65",
        "id": 615495918,
        "node_id": "MDU6SXNzdWU2MTU0OTU5MTg=",
        "number": 65,
        "title": "Contracts as a CoffeeScript plugin",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 0,
        "created_at": "2020-05-10T23:02:15Z",
        "updated_at": "2020-05-10T23:02:15Z",
        "closed_at": null,
        "author_association": "NONE",
        "body": "Hi @disnet,\r\n\r\nWhat about to share your knowledge on the [barista discussion](https://github.com/jashkenas/coffeescript/issues/5320) and bring `contracts.coffee` to the future CoffeeScript versions?"
      }
    },
    "public": true,
    "created_at": "2020-05-10T23:02:16Z"
  },
  {
    "id": "12294107123",
    "type": "IssuesEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "opened",
      "issue": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320",
        "repository_url": "https://api.github.com/repos/jashkenas/coffeescript",
        "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320/labels{/name}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320/comments",
        "events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5320/events",
        "html_url": "https://github.com/jashkenas/coffeescript/issues/5320",
        "id": 615487193,
        "node_id": "MDU6SXNzdWU2MTU0ODcxOTM=",
        "number": 5320,
        "title": "Discussion: Barista, the Pluggable CoffeeScript",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 0,
        "created_at": "2020-05-10T22:09:33Z",
        "updated_at": "2020-05-10T22:09:33Z",
        "closed_at": null,
        "author_association": "NONE",
        "body": "## Barista, who can build new flavors of coffee for you.\r\n\r\nIt looks like we agree that is interesting to have a pluggable coffee compiler in the \"[TypeScript Output](https://github.com/jashkenas/coffeescript/issues/5307)\" discussion, for a health evolution of the language and the CoffeeScript ecosystem.\r\n\r\nAs I understand, the CoffeeScript source is a elaborated [Jison](http://zaa.ch/jison) configuration, and the compiler itself is a parser code generated by Jison. Make this generated code pluggable can be hard to do and harder to maintain.\r\n\r\nI believe, the right path to make a pluggable coffee compiler is to have `jison` as a execution dependency and the compiler itself must to be build at runtime.\r\n\r\nWell it obviously will make the coffee compiler heavier and slower.\r\n\r\nSo I propose a new build task on the Cakefile to create the **\"barista compiler\"**. It will be the same CoffeeScript compiler source, but not generated by Jison, its API will have plugin related configs, as its bin's CLI will have some plugins args. And, sure, it must be shipped with a different `package.json` with `\"jison\"` in the `\"dependencies\"` list.\r\n\r\n## How a plugin may work\r\n\r\nA plugin must implement methods to parameterize, extend or replace the transpiler units:\r\n  * `Lexer.tokenize()`: Looks like it will be the harder process to become pluggable. The method itself may stay untouchable by plugins, but they must to be able to change or parameterize its specialized tokenizers (`identifierToken()`, `stringToken()`, ...) or add new ones on any position of the [tokenizers call line](https://github.com/jashkenas/coffeescript/blob/75d376f2eff7108ea8bb91d4326aac62989c8556/src/lexer.coffee#L69-L80).\r\n  * `grammar` must be exported as it is and freely modifiable by the plugin code.\r\n  * `nodes` module looks like the most important is already exported and can be changed by the plugin. But it need to be clear where a new collection of node classes must to be attached to be used by the extended transpiler. Some `nodes.coffee` helpers must to be exported to become usable by new node classes, and it must export a method to enable the plugins to replace this helpers if needed.\r\n\r\n```\r\n┌────────────────────────────────────────────────────────┐\r\n│ barista + plugin1 + plugin2 + user-src.flavored-coffee │\r\n└────────────────────────────────────────────────────────┘\r\n                          ⇩\r\n              ╔════════════════════════╗\r\n              ║    Barista Compiler    ║\r\n              ╟────────────────────────╢\r\n              ║  ┌──────────────────┐  ║\r\n              ║  │ CoffeeScript Src ├┐ ║\r\n              ║  └┬─────────────────┘│ ║\r\n              ║   └──────────────────┘ ║\r\n              ║           ⇩            ║\r\n              ║  ┏━━━━━━━━━━━━━━━━━━┓  ║\r\n              ║  ┃     Plugin 1     ┃  ║\r\n              ║  ┗━━━━━━━━━━━━━━━━━━┛  ║\r\n              ║           ⇩            ║\r\n              ║  ┏━━━━━━━━━━━━━━━━━━┓  ║\r\n              ║  ┃     Plugin 2     ┃  ║\r\n              ║  ┗━━━━━━━━━━━━━━━━━━┛  ║\r\n              ║           ⇩            ║\r\n              ║  ╔══════════════════╗  ║\r\n              ║  ║  Cache Flavored  ║  ║\r\n              ║  ║   CoffeeScript   ║  ║\r\n              ║  ║    Transpiler    ║  ║\r\n              ║  ╚══════════════════╝  ║\r\n              ╚════════════════════════╝\r\n                          ⇩\r\n              ┌───────────────────────┐\r\n              │  user-src.transpiled  │\r\n              └───────────────────────┘\r\n```\r\n\r\nWhat you think? What is missed? Is there a better path? Do you know specific details that need to be described?"
      }
    },
    "public": true,
    "created_at": "2020-05-10T22:09:34Z"
  },
  {
    "id": "12293534284",
    "type": "IssueCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "created",
      "issue": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5317",
        "repository_url": "https://api.github.com/repos/jashkenas/coffeescript",
        "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5317/labels{/name}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5317/comments",
        "events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5317/events",
        "html_url": "https://github.com/jashkenas/coffeescript/issues/5317",
        "id": 611299249,
        "node_id": "MDU6SXNzdWU2MTEyOTkyNDk=",
        "number": 5317,
        "title": "Grammar, Nodes: Support import.meta and import.meta.*",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [
          {
            "id": 18988,
            "node_id": "MDU6TGFiZWwxODk4OA==",
            "url": "https://api.github.com/repos/jashkenas/coffeescript/labels/enhancement",
            "name": "enhancement",
            "color": "84b6eb",
            "default": true,
            "description": null
          },
          {
            "id": 717622434,
            "node_id": "MDU6TGFiZWw3MTc2MjI0MzQ=",
            "url": "https://api.github.com/repos/jashkenas/coffeescript/labels/help%20wanted",
            "name": "help wanted",
            "color": "fbca04",
            "default": true,
            "description": null
          }
        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 5,
        "created_at": "2020-05-03T00:02:12Z",
        "updated_at": "2020-05-10T19:26:38Z",
        "closed_at": null,
        "author_association": "NONE",
        "body": "### Expected Behavior\r\n`import.meta.url` should return module's full path in a `file://` protocol.\r\n\r\n### Current Behavior\r\n```\r\n$ coffee -pe 'dirname = path.dirname import.meta.url'\r\n[stdin]:1:24: error: unexpected import\r\ndirname = path.dirname import.meta.url\r\n                       ^^^^^^\r\n$ coffee -pe 'dirname = path.dirname(import.meta.url)'\r\n[stdin]:1:30: error: unexpected .\r\ndirname = path.dirname(import.meta.url)\r\n                             ^\r\n```\r\n### Workarrownd\r\nWe can workarrownd it with backticks\r\n```coffee\r\ndirname = path.dirname `import.meta.url`\r\n```\r\n...however it is not good to the language.\r\n\r\n### Context\r\n`import.meta.url` is necessary source of information to replace `__(dir|file)name` features.\r\n\r\n### Environment\r\n* CoffeeScript version: 2.5.1\r\n* Node.js version: 14.1.0\r\n"
      },
      "comment": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/issues/comments/626376953",
        "html_url": "https://github.com/jashkenas/coffeescript/issues/5317#issuecomment-626376953",
        "issue_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5317",
        "id": 626376953,
        "node_id": "MDEyOklzc3VlQ29tbWVudDYyNjM3Njk1Mw==",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "created_at": "2020-05-10T19:26:38Z",
        "updated_at": "2020-05-10T19:26:38Z",
        "author_association": "NONE",
        "body": "@GeoffreyBooth, Good News!\r\nI'm very grateful for your help. I almost gave up and would use the `IDENTIFIER` token, nevertheless I managed to define a specific token for `import.meta`."
      }
    },
    "public": true,
    "created_at": "2020-05-10T19:26:39Z"
  },
  {
    "id": "12293510797",
    "type": "PullRequestEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 441269,
      "name": "jashkenas/coffeescript",
      "url": "https://api.github.com/repos/jashkenas/coffeescript"
    },
    "payload": {
      "action": "opened",
      "number": 5319,
      "pull_request": {
        "url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319",
        "id": 415767768,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NzY3NzY4",
        "html_url": "https://github.com/jashkenas/coffeescript/pull/5319",
        "diff_url": "https://github.com/jashkenas/coffeescript/pull/5319.diff",
        "patch_url": "https://github.com/jashkenas/coffeescript/pull/5319.patch",
        "issue_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319",
        "number": 5319,
        "state": "open",
        "locked": false,
        "title": "Support import.meta and import.meta.*",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Allows to access `import.meta` and only if its property is `meta`.\r\n\r\nThe new token `IMPORT_META` was created to allow richer formatting and code analysis.\r\n```\r\n$ ./bin/coffee --tokens -e 'import.meta.something'\r\n[IMPORT_META import] [. .] [PROPERTY meta] [. .] [PROPERTY something] [TERMINATOR \\n]\r\n```\r\ncloses #5317",
        "created_at": "2020-05-10T19:20:08Z",
        "updated_at": "2020-05-10T19:20:08Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": null,
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/commits",
        "review_comments_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/comments",
        "review_comment_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments",
        "statuses_url": "https://api.github.com/repos/jashkenas/coffeescript/statuses/ebba62c2a3c4fac2ddaeaadac0cb2649a065628b",
        "head": {
          "label": "aurium:master",
          "ref": "master",
          "sha": "ebba62c2a3c4fac2ddaeaadac0cb2649a065628b",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 261903793,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNjE5MDM3OTM=",
            "name": "coffeescript",
            "full_name": "aurium/coffeescript",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/coffeescript",
            "description": "Unfancy JavaScript",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/coffeescript",
            "forks_url": "https://api.github.com/repos/aurium/coffeescript/forks",
            "keys_url": "https://api.github.com/repos/aurium/coffeescript/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/coffeescript/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/coffeescript/teams",
            "hooks_url": "https://api.github.com/repos/aurium/coffeescript/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/coffeescript/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/coffeescript/events",
            "assignees_url": "https://api.github.com/repos/aurium/coffeescript/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/coffeescript/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/coffeescript/tags",
            "blobs_url": "https://api.github.com/repos/aurium/coffeescript/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/coffeescript/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/coffeescript/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/coffeescript/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/coffeescript/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/coffeescript/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/coffeescript/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/coffeescript/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/coffeescript/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/coffeescript/subscription",
            "commits_url": "https://api.github.com/repos/aurium/coffeescript/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/coffeescript/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/coffeescript/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/coffeescript/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/coffeescript/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/coffeescript/merges",
            "archive_url": "https://api.github.com/repos/aurium/coffeescript/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/coffeescript/downloads",
            "issues_url": "https://api.github.com/repos/aurium/coffeescript/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/coffeescript/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/coffeescript/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/coffeescript/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/coffeescript/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/coffeescript/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/coffeescript/deployments",
            "created_at": "2020-05-06T23:40:26Z",
            "updated_at": "2020-05-10T19:09:45Z",
            "pushed_at": "2020-05-10T19:09:40Z",
            "git_url": "git://github.com/aurium/coffeescript.git",
            "ssh_url": "git@github.com:aurium/coffeescript.git",
            "clone_url": "https://github.com/aurium/coffeescript.git",
            "svn_url": "https://github.com/aurium/coffeescript",
            "homepage": "https://coffeescript.org/",
            "size": 29292,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "CoffeeScript",
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "jashkenas:master",
          "ref": "master",
          "sha": "75d376f2eff7108ea8bb91d4326aac62989c8556",
          "user": {
            "login": "jashkenas",
            "id": 4732,
            "node_id": "MDQ6VXNlcjQ3MzI=",
            "avatar_url": "https://avatars3.githubusercontent.com/u/4732?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/jashkenas",
            "html_url": "https://github.com/jashkenas",
            "followers_url": "https://api.github.com/users/jashkenas/followers",
            "following_url": "https://api.github.com/users/jashkenas/following{/other_user}",
            "gists_url": "https://api.github.com/users/jashkenas/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/jashkenas/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/jashkenas/subscriptions",
            "organizations_url": "https://api.github.com/users/jashkenas/orgs",
            "repos_url": "https://api.github.com/users/jashkenas/repos",
            "events_url": "https://api.github.com/users/jashkenas/events{/privacy}",
            "received_events_url": "https://api.github.com/users/jashkenas/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 441269,
            "node_id": "MDEwOlJlcG9zaXRvcnk0NDEyNjk=",
            "name": "coffeescript",
            "full_name": "jashkenas/coffeescript",
            "private": false,
            "owner": {
              "login": "jashkenas",
              "id": 4732,
              "node_id": "MDQ6VXNlcjQ3MzI=",
              "avatar_url": "https://avatars3.githubusercontent.com/u/4732?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/jashkenas",
              "html_url": "https://github.com/jashkenas",
              "followers_url": "https://api.github.com/users/jashkenas/followers",
              "following_url": "https://api.github.com/users/jashkenas/following{/other_user}",
              "gists_url": "https://api.github.com/users/jashkenas/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/jashkenas/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/jashkenas/subscriptions",
              "organizations_url": "https://api.github.com/users/jashkenas/orgs",
              "repos_url": "https://api.github.com/users/jashkenas/repos",
              "events_url": "https://api.github.com/users/jashkenas/events{/privacy}",
              "received_events_url": "https://api.github.com/users/jashkenas/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/jashkenas/coffeescript",
            "description": "Unfancy JavaScript",
            "fork": false,
            "url": "https://api.github.com/repos/jashkenas/coffeescript",
            "forks_url": "https://api.github.com/repos/jashkenas/coffeescript/forks",
            "keys_url": "https://api.github.com/repos/jashkenas/coffeescript/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/jashkenas/coffeescript/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/jashkenas/coffeescript/teams",
            "hooks_url": "https://api.github.com/repos/jashkenas/coffeescript/hooks",
            "issue_events_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/events{/number}",
            "events_url": "https://api.github.com/repos/jashkenas/coffeescript/events",
            "assignees_url": "https://api.github.com/repos/jashkenas/coffeescript/assignees{/user}",
            "branches_url": "https://api.github.com/repos/jashkenas/coffeescript/branches{/branch}",
            "tags_url": "https://api.github.com/repos/jashkenas/coffeescript/tags",
            "blobs_url": "https://api.github.com/repos/jashkenas/coffeescript/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/jashkenas/coffeescript/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/jashkenas/coffeescript/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/jashkenas/coffeescript/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/jashkenas/coffeescript/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/jashkenas/coffeescript/languages",
            "stargazers_url": "https://api.github.com/repos/jashkenas/coffeescript/stargazers",
            "contributors_url": "https://api.github.com/repos/jashkenas/coffeescript/contributors",
            "subscribers_url": "https://api.github.com/repos/jashkenas/coffeescript/subscribers",
            "subscription_url": "https://api.github.com/repos/jashkenas/coffeescript/subscription",
            "commits_url": "https://api.github.com/repos/jashkenas/coffeescript/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/jashkenas/coffeescript/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/jashkenas/coffeescript/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/jashkenas/coffeescript/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/jashkenas/coffeescript/contents/{+path}",
            "compare_url": "https://api.github.com/repos/jashkenas/coffeescript/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/jashkenas/coffeescript/merges",
            "archive_url": "https://api.github.com/repos/jashkenas/coffeescript/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/jashkenas/coffeescript/downloads",
            "issues_url": "https://api.github.com/repos/jashkenas/coffeescript/issues{/number}",
            "pulls_url": "https://api.github.com/repos/jashkenas/coffeescript/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/jashkenas/coffeescript/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/jashkenas/coffeescript/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/jashkenas/coffeescript/labels{/name}",
            "releases_url": "https://api.github.com/repos/jashkenas/coffeescript/releases{/id}",
            "deployments_url": "https://api.github.com/repos/jashkenas/coffeescript/deployments",
            "created_at": "2009-12-18T01:39:53Z",
            "updated_at": "2020-05-10T18:35:14Z",
            "pushed_at": "2020-03-10T17:29:02Z",
            "git_url": "git://github.com/jashkenas/coffeescript.git",
            "ssh_url": "git@github.com:jashkenas/coffeescript.git",
            "clone_url": "https://github.com/jashkenas/coffeescript.git",
            "svn_url": "https://github.com/jashkenas/coffeescript",
            "homepage": "https://coffeescript.org/",
            "size": 29192,
            "stargazers_count": 15535,
            "watchers_count": 15535,
            "language": "CoffeeScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": true,
            "forks_count": 2006,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 46,
            "license": {
              "key": "mit",
              "name": "MIT License",
              "spdx_id": "MIT",
              "url": "https://api.github.com/licenses/mit",
              "node_id": "MDc6TGljZW5zZTEz"
            },
            "forks": 2006,
            "open_issues": 46,
            "watchers": 15535,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319"
          },
          "html": {
            "href": "https://github.com/jashkenas/coffeescript/pull/5319"
          },
          "issue": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319"
          },
          "comments": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/issues/5319/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/pulls/5319/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/jashkenas/coffeescript/statuses/ebba62c2a3c4fac2ddaeaadac0cb2649a065628b"
          }
        },
        "author_association": "NONE",
        "merged": false,
        "mergeable": null,
        "rebaseable": null,
        "mergeable_state": "unknown",
        "merged_by": null,
        "comments": 0,
        "review_comments": 0,
        "maintainer_can_modify": true,
        "commits": 2,
        "additions": 202,
        "deletions": 159,
        "changed_files": 8
      }
    },
    "public": true,
    "created_at": "2020-05-10T19:20:09Z"
  },
  {
    "id": "12293471918",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 261903793,
      "name": "aurium/coffeescript",
      "url": "https://api.github.com/repos/aurium/coffeescript"
    },
    "payload": {
      "push_id": 5049779125,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/master",
      "head": "ebba62c2a3c4fac2ddaeaadac0cb2649a065628b",
      "before": "788540a0549141b6c84e0dc5dad150a8c278c6e9",
      "commits": [
        {
          "sha": "ebba62c2a3c4fac2ddaeaadac0cb2649a065628b",
          "author": {
            "email": "aurium@gmail.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Fix: Support import.meta and import.meta.*\n\nIt adds a new token `IMPORT_META` allowing richer formatting and code analysis.",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript/commits/ebba62c2a3c4fac2ddaeaadac0cb2649a065628b"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-10T19:09:44Z"
  },
  {
    "id": "12293191711",
    "type": "PullRequestEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 64283764,
      "name": "GeoffreyBooth/coffeescript-gulp",
      "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp"
    },
    "payload": {
      "action": "opened",
      "number": 2,
      "pull_request": {
        "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/2",
        "id": 415756378,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NzU2Mzc4",
        "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/2",
        "diff_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/2.diff",
        "patch_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/2.patch",
        "issue_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/2",
        "number": 2,
        "state": "open",
        "locked": false,
        "title": "Don't miss a file when reseting without parser.",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Not only simpler, but also more tolerant to changes in the CoffeeScript project.",
        "created_at": "2020-05-10T17:52:41Z",
        "updated_at": "2020-05-10T17:52:41Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": null,
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/2/commits",
        "review_comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/2/comments",
        "review_comment_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/2/comments",
        "statuses_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/5b0b501dfedc3b4793b3cc0f5d7461aacfeff43a",
        "head": {
          "label": "aurium:patch-2",
          "ref": "patch-2",
          "sha": "5b0b501dfedc3b4793b3cc0f5d7461aacfeff43a",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 262469253,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNjI0NjkyNTM=",
            "name": "coffeescript-gulp",
            "full_name": "aurium/coffeescript-gulp",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/coffeescript-gulp",
            "description": "Gulp task to watch CoffeeScript source files and recompile",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/coffeescript-gulp",
            "forks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/forks",
            "keys_url": "https://api.github.com/repos/aurium/coffeescript-gulp/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/coffeescript-gulp/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/coffeescript-gulp/teams",
            "hooks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/events",
            "assignees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/coffeescript-gulp/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/tags",
            "blobs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/coffeescript-gulp/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/coffeescript-gulp/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscription",
            "commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/coffeescript-gulp/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/coffeescript-gulp/merges",
            "archive_url": "https://api.github.com/repos/aurium/coffeescript-gulp/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/coffeescript-gulp/downloads",
            "issues_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/coffeescript-gulp/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/coffeescript-gulp/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/coffeescript-gulp/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/coffeescript-gulp/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/coffeescript-gulp/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/deployments",
            "created_at": "2020-05-09T02:10:27Z",
            "updated_at": "2020-05-09T02:10:29Z",
            "pushed_at": "2020-05-10T17:52:26Z",
            "git_url": "git://github.com/aurium/coffeescript-gulp.git",
            "ssh_url": "git@github.com:aurium/coffeescript-gulp.git",
            "clone_url": "https://github.com/aurium/coffeescript-gulp.git",
            "svn_url": "https://github.com/aurium/coffeescript-gulp",
            "homepage": null,
            "size": 48,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": null,
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": null,
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "GeoffreyBooth:master",
          "ref": "master",
          "sha": "472fd8a8ea49ade5dee892463f618ecbb5dcdc65",
          "user": {
            "login": "GeoffreyBooth",
            "id": 456802,
            "node_id": "MDQ6VXNlcjQ1NjgwMg==",
            "avatar_url": "https://avatars2.githubusercontent.com/u/456802?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/GeoffreyBooth",
            "html_url": "https://github.com/GeoffreyBooth",
            "followers_url": "https://api.github.com/users/GeoffreyBooth/followers",
            "following_url": "https://api.github.com/users/GeoffreyBooth/following{/other_user}",
            "gists_url": "https://api.github.com/users/GeoffreyBooth/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/GeoffreyBooth/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/GeoffreyBooth/subscriptions",
            "organizations_url": "https://api.github.com/users/GeoffreyBooth/orgs",
            "repos_url": "https://api.github.com/users/GeoffreyBooth/repos",
            "events_url": "https://api.github.com/users/GeoffreyBooth/events{/privacy}",
            "received_events_url": "https://api.github.com/users/GeoffreyBooth/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 64283764,
            "node_id": "MDEwOlJlcG9zaXRvcnk2NDI4Mzc2NA==",
            "name": "coffeescript-gulp",
            "full_name": "GeoffreyBooth/coffeescript-gulp",
            "private": false,
            "owner": {
              "login": "GeoffreyBooth",
              "id": 456802,
              "node_id": "MDQ6VXNlcjQ1NjgwMg==",
              "avatar_url": "https://avatars2.githubusercontent.com/u/456802?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/GeoffreyBooth",
              "html_url": "https://github.com/GeoffreyBooth",
              "followers_url": "https://api.github.com/users/GeoffreyBooth/followers",
              "following_url": "https://api.github.com/users/GeoffreyBooth/following{/other_user}",
              "gists_url": "https://api.github.com/users/GeoffreyBooth/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/GeoffreyBooth/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/GeoffreyBooth/subscriptions",
              "organizations_url": "https://api.github.com/users/GeoffreyBooth/orgs",
              "repos_url": "https://api.github.com/users/GeoffreyBooth/repos",
              "events_url": "https://api.github.com/users/GeoffreyBooth/events{/privacy}",
              "received_events_url": "https://api.github.com/users/GeoffreyBooth/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp",
            "description": "Gulp task to watch CoffeeScript source files and recompile",
            "fork": false,
            "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp",
            "forks_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/forks",
            "keys_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/teams",
            "hooks_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/hooks",
            "issue_events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/events{/number}",
            "events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/events",
            "assignees_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/assignees{/user}",
            "branches_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/branches{/branch}",
            "tags_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/tags",
            "blobs_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/languages",
            "stargazers_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/stargazers",
            "contributors_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/contributors",
            "subscribers_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/subscribers",
            "subscription_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/subscription",
            "commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/contents/{+path}",
            "compare_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/merges",
            "archive_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/downloads",
            "issues_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues{/number}",
            "pulls_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/labels{/name}",
            "releases_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/releases{/id}",
            "deployments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/deployments",
            "created_at": "2016-07-27T06:36:35Z",
            "updated_at": "2020-05-07T19:14:54Z",
            "pushed_at": "2020-05-10T17:34:53Z",
            "git_url": "git://github.com/GeoffreyBooth/coffeescript-gulp.git",
            "ssh_url": "git@github.com:GeoffreyBooth/coffeescript-gulp.git",
            "clone_url": "https://github.com/GeoffreyBooth/coffeescript-gulp.git",
            "svn_url": "https://github.com/GeoffreyBooth/coffeescript-gulp",
            "homepage": null,
            "size": 47,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "CoffeeScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 1,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 2,
            "license": null,
            "forks": 1,
            "open_issues": 2,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/2"
          },
          "html": {
            "href": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/2"
          },
          "issue": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/2"
          },
          "comments": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/2/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/2/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/2/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/5b0b501dfedc3b4793b3cc0f5d7461aacfeff43a"
          }
        },
        "author_association": "NONE",
        "merged": false,
        "mergeable": null,
        "rebaseable": null,
        "mergeable_state": "unknown",
        "merged_by": null,
        "comments": 0,
        "review_comments": 0,
        "maintainer_can_modify": true,
        "commits": 1,
        "additions": 4,
        "deletions": 17,
        "changed_files": 1
      }
    },
    "public": true,
    "created_at": "2020-05-10T17:52:41Z"
  },
  {
    "id": "12293190799",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 262469253,
      "name": "aurium/coffeescript-gulp",
      "url": "https://api.github.com/repos/aurium/coffeescript-gulp"
    },
    "payload": {
      "push_id": 5049595113,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/patch-2",
      "head": "5b0b501dfedc3b4793b3cc0f5d7461aacfeff43a",
      "before": "472fd8a8ea49ade5dee892463f618ecbb5dcdc65",
      "commits": [
        {
          "sha": "5b0b501dfedc3b4793b3cc0f5d7461aacfeff43a",
          "author": {
            "email": "30254+aurium@users.noreply.github.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Don't miss a file when reseting without parser.\n\nNot only simpler, but also more tolerant to changes in the CoffeeScript project.",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits/5b0b501dfedc3b4793b3cc0f5d7461aacfeff43a"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-10T17:52:27Z"
  },
  {
    "id": "12293126376",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 262469253,
      "name": "aurium/coffeescript-gulp",
      "url": "https://api.github.com/repos/aurium/coffeescript-gulp"
    },
    "payload": {
      "push_id": 5049552703,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/patch-1",
      "head": "e199f835f95b09856ff059ae16f17934562cb5a0",
      "before": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
      "commits": [
        {
          "sha": "e199f835f95b09856ff059ae16f17934562cb5a0",
          "author": {
            "email": "30254+aurium@users.noreply.github.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Clear and Sync as one shell exec",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits/e199f835f95b09856ff059ae16f17934562cb5a0"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-10T17:34:52Z"
  },
  {
    "id": "12293117509",
    "type": "PullRequestReviewCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 64283764,
      "name": "GeoffreyBooth/coffeescript-gulp",
      "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp"
    },
    "payload": {
      "action": "created",
      "comment": {
        "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments/422675025",
        "pull_request_review_id": 408765473,
        "id": 422675025,
        "node_id": "MDI0OlB1bGxSZXF1ZXN0UmV2aWV3Q29tbWVudDQyMjY3NTAyNQ==",
        "diff_hunk": "@@ -9,6 +9,7 @@ execSyncOptions =\n buildAndTest = (done, includingParser = no) ->\n \ttry\n \t\texecSync \"clear; printf '\\\\033[3J'\", execSyncOptions\n+\t\texecSync 'sync', execSyncOptions",
        "path": "gulpfile.coffee",
        "position": 4,
        "original_position": 4,
        "commit_id": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
        "original_commit_id": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "With `&&` rather than `;` we will not to suffer with silence fail. 🙂\r\n```suggestion\r\n\t\texecSync \"clear && sync && printf '\\\\033[3J'\", execSyncOptions\r\n```",
        "created_at": "2020-05-10T17:32:26Z",
        "updated_at": "2020-05-10T17:32:26Z",
        "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1#discussion_r422675025",
        "pull_request_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1",
        "author_association": "NONE",
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments/422675025"
          },
          "html": {
            "href": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1#discussion_r422675025"
          },
          "pull_request": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1"
          }
        },
        "start_line": 11,
        "original_start_line": 11,
        "start_side": "RIGHT",
        "line": 12,
        "original_line": 12,
        "side": "RIGHT",
        "in_reply_to_id": 422443802
      },
      "pull_request": {
        "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1",
        "id": 415499609,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NDk5NjA5",
        "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1",
        "diff_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1.diff",
        "patch_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1.patch",
        "issue_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1",
        "number": 1,
        "state": "open",
        "locked": false,
        "title": "Sync FS before build",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Ensure the file changes will be accessible by the builder process.",
        "created_at": "2020-05-09T02:18:21Z",
        "updated_at": "2020-05-10T17:32:26Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": "2c6369150d6dad8774f88d4b12b6becd5e240752",
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/commits",
        "review_comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/comments",
        "review_comment_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/comments",
        "statuses_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
        "head": {
          "label": "aurium:patch-1",
          "ref": "patch-1",
          "sha": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 262469253,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNjI0NjkyNTM=",
            "name": "coffeescript-gulp",
            "full_name": "aurium/coffeescript-gulp",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/coffeescript-gulp",
            "description": "Gulp task to watch CoffeeScript source files and recompile",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/coffeescript-gulp",
            "forks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/forks",
            "keys_url": "https://api.github.com/repos/aurium/coffeescript-gulp/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/coffeescript-gulp/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/coffeescript-gulp/teams",
            "hooks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/events",
            "assignees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/coffeescript-gulp/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/tags",
            "blobs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/coffeescript-gulp/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/coffeescript-gulp/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscription",
            "commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/coffeescript-gulp/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/coffeescript-gulp/merges",
            "archive_url": "https://api.github.com/repos/aurium/coffeescript-gulp/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/coffeescript-gulp/downloads",
            "issues_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/coffeescript-gulp/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/coffeescript-gulp/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/coffeescript-gulp/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/coffeescript-gulp/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/coffeescript-gulp/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/deployments",
            "created_at": "2020-05-09T02:10:27Z",
            "updated_at": "2020-05-09T02:10:29Z",
            "pushed_at": "2020-05-09T02:17:41Z",
            "git_url": "git://github.com/aurium/coffeescript-gulp.git",
            "ssh_url": "git@github.com:aurium/coffeescript-gulp.git",
            "clone_url": "https://github.com/aurium/coffeescript-gulp.git",
            "svn_url": "https://github.com/aurium/coffeescript-gulp",
            "homepage": null,
            "size": 48,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": null,
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": null,
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "GeoffreyBooth:master",
          "ref": "master",
          "sha": "472fd8a8ea49ade5dee892463f618ecbb5dcdc65",
          "user": {
            "login": "GeoffreyBooth",
            "id": 456802,
            "node_id": "MDQ6VXNlcjQ1NjgwMg==",
            "avatar_url": "https://avatars2.githubusercontent.com/u/456802?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/GeoffreyBooth",
            "html_url": "https://github.com/GeoffreyBooth",
            "followers_url": "https://api.github.com/users/GeoffreyBooth/followers",
            "following_url": "https://api.github.com/users/GeoffreyBooth/following{/other_user}",
            "gists_url": "https://api.github.com/users/GeoffreyBooth/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/GeoffreyBooth/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/GeoffreyBooth/subscriptions",
            "organizations_url": "https://api.github.com/users/GeoffreyBooth/orgs",
            "repos_url": "https://api.github.com/users/GeoffreyBooth/repos",
            "events_url": "https://api.github.com/users/GeoffreyBooth/events{/privacy}",
            "received_events_url": "https://api.github.com/users/GeoffreyBooth/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 64283764,
            "node_id": "MDEwOlJlcG9zaXRvcnk2NDI4Mzc2NA==",
            "name": "coffeescript-gulp",
            "full_name": "GeoffreyBooth/coffeescript-gulp",
            "private": false,
            "owner": {
              "login": "GeoffreyBooth",
              "id": 456802,
              "node_id": "MDQ6VXNlcjQ1NjgwMg==",
              "avatar_url": "https://avatars2.githubusercontent.com/u/456802?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/GeoffreyBooth",
              "html_url": "https://github.com/GeoffreyBooth",
              "followers_url": "https://api.github.com/users/GeoffreyBooth/followers",
              "following_url": "https://api.github.com/users/GeoffreyBooth/following{/other_user}",
              "gists_url": "https://api.github.com/users/GeoffreyBooth/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/GeoffreyBooth/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/GeoffreyBooth/subscriptions",
              "organizations_url": "https://api.github.com/users/GeoffreyBooth/orgs",
              "repos_url": "https://api.github.com/users/GeoffreyBooth/repos",
              "events_url": "https://api.github.com/users/GeoffreyBooth/events{/privacy}",
              "received_events_url": "https://api.github.com/users/GeoffreyBooth/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp",
            "description": "Gulp task to watch CoffeeScript source files and recompile",
            "fork": false,
            "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp",
            "forks_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/forks",
            "keys_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/teams",
            "hooks_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/hooks",
            "issue_events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/events{/number}",
            "events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/events",
            "assignees_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/assignees{/user}",
            "branches_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/branches{/branch}",
            "tags_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/tags",
            "blobs_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/languages",
            "stargazers_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/stargazers",
            "contributors_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/contributors",
            "subscribers_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/subscribers",
            "subscription_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/subscription",
            "commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/contents/{+path}",
            "compare_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/merges",
            "archive_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/downloads",
            "issues_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues{/number}",
            "pulls_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/labels{/name}",
            "releases_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/releases{/id}",
            "deployments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/deployments",
            "created_at": "2016-07-27T06:36:35Z",
            "updated_at": "2020-05-07T19:14:54Z",
            "pushed_at": "2020-05-09T02:18:22Z",
            "git_url": "git://github.com/GeoffreyBooth/coffeescript-gulp.git",
            "ssh_url": "git@github.com:GeoffreyBooth/coffeescript-gulp.git",
            "clone_url": "https://github.com/GeoffreyBooth/coffeescript-gulp.git",
            "svn_url": "https://github.com/GeoffreyBooth/coffeescript-gulp",
            "homepage": null,
            "size": 47,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "CoffeeScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 1,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 1,
            "license": null,
            "forks": 1,
            "open_issues": 1,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1"
          },
          "html": {
            "href": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1"
          },
          "issue": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1"
          },
          "comments": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9"
          }
        },
        "author_association": "NONE"
      }
    },
    "public": true,
    "created_at": "2020-05-10T17:32:26Z"
  },
  {
    "id": "12293054495",
    "type": "IssueCommentEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 64283764,
      "name": "GeoffreyBooth/coffeescript-gulp",
      "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp"
    },
    "payload": {
      "action": "created",
      "issue": {
        "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1",
        "repository_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp",
        "labels_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/labels{/name}",
        "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/comments",
        "events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/events",
        "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1",
        "id": 615070052,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NDk5NjA5",
        "number": 1,
        "title": "Sync FS before build",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [

        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [

        ],
        "milestone": null,
        "comments": 2,
        "created_at": "2020-05-09T02:18:21Z",
        "updated_at": "2020-05-10T17:15:51Z",
        "closed_at": null,
        "author_association": "NONE",
        "pull_request": {
          "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1",
          "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1",
          "diff_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1.diff",
          "patch_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1.patch"
        },
        "body": "Ensure the file changes will be accessible by the builder process."
      },
      "comment": {
        "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/comments/626359934",
        "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1#issuecomment-626359934",
        "issue_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1",
        "id": 626359934,
        "node_id": "MDEyOklzc3VlQ29tbWVudDYyNjM1OTkzNA==",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "created_at": "2020-05-10T17:15:51Z",
        "updated_at": "2020-05-10T17:15:51Z",
        "author_association": "NONE",
        "body": "Oh yes. I have this trouble with listening for FS events, then build something in two different Linux flavors (Debian and Mint). It is often that the event is triggered just after saving a file, however the build reads the previews version from FS. That looks like a cache issue, because the `sync` only function is to push the FS cache to the hardware."
      }
    },
    "public": true,
    "created_at": "2020-05-10T17:15:51Z"
  },
  {
    "id": "12285558650",
    "type": "PullRequestEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 64283764,
      "name": "GeoffreyBooth/coffeescript-gulp",
      "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp"
    },
    "payload": {
      "action": "opened",
      "number": 1,
      "pull_request": {
        "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1",
        "id": 415499609,
        "node_id": "MDExOlB1bGxSZXF1ZXN0NDE1NDk5NjA5",
        "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1",
        "diff_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1.diff",
        "patch_url": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1.patch",
        "issue_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1",
        "number": 1,
        "state": "open",
        "locked": false,
        "title": "Sync FS before build",
        "user": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "body": "Ensure the file changes will be accessible by the builder process.",
        "created_at": "2020-05-09T02:18:21Z",
        "updated_at": "2020-05-09T02:18:21Z",
        "closed_at": null,
        "merged_at": null,
        "merge_commit_sha": null,
        "assignee": null,
        "assignees": [

        ],
        "requested_reviewers": [

        ],
        "requested_teams": [

        ],
        "labels": [

        ],
        "milestone": null,
        "draft": false,
        "commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/commits",
        "review_comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/comments",
        "review_comment_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments{/number}",
        "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/comments",
        "statuses_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
        "head": {
          "label": "aurium:patch-1",
          "ref": "patch-1",
          "sha": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
          "user": {
            "login": "aurium",
            "id": 30254,
            "node_id": "MDQ6VXNlcjMwMjU0",
            "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/aurium",
            "html_url": "https://github.com/aurium",
            "followers_url": "https://api.github.com/users/aurium/followers",
            "following_url": "https://api.github.com/users/aurium/following{/other_user}",
            "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
            "organizations_url": "https://api.github.com/users/aurium/orgs",
            "repos_url": "https://api.github.com/users/aurium/repos",
            "events_url": "https://api.github.com/users/aurium/events{/privacy}",
            "received_events_url": "https://api.github.com/users/aurium/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 262469253,
            "node_id": "MDEwOlJlcG9zaXRvcnkyNjI0NjkyNTM=",
            "name": "coffeescript-gulp",
            "full_name": "aurium/coffeescript-gulp",
            "private": false,
            "owner": {
              "login": "aurium",
              "id": 30254,
              "node_id": "MDQ6VXNlcjMwMjU0",
              "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/aurium",
              "html_url": "https://github.com/aurium",
              "followers_url": "https://api.github.com/users/aurium/followers",
              "following_url": "https://api.github.com/users/aurium/following{/other_user}",
              "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
              "organizations_url": "https://api.github.com/users/aurium/orgs",
              "repos_url": "https://api.github.com/users/aurium/repos",
              "events_url": "https://api.github.com/users/aurium/events{/privacy}",
              "received_events_url": "https://api.github.com/users/aurium/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/aurium/coffeescript-gulp",
            "description": "Gulp task to watch CoffeeScript source files and recompile",
            "fork": true,
            "url": "https://api.github.com/repos/aurium/coffeescript-gulp",
            "forks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/forks",
            "keys_url": "https://api.github.com/repos/aurium/coffeescript-gulp/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/aurium/coffeescript-gulp/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/aurium/coffeescript-gulp/teams",
            "hooks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/hooks",
            "issue_events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/events{/number}",
            "events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/events",
            "assignees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/assignees{/user}",
            "branches_url": "https://api.github.com/repos/aurium/coffeescript-gulp/branches{/branch}",
            "tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/tags",
            "blobs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/aurium/coffeescript-gulp/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/aurium/coffeescript-gulp/languages",
            "stargazers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/stargazers",
            "contributors_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contributors",
            "subscribers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscribers",
            "subscription_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscription",
            "commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contents/{+path}",
            "compare_url": "https://api.github.com/repos/aurium/coffeescript-gulp/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/aurium/coffeescript-gulp/merges",
            "archive_url": "https://api.github.com/repos/aurium/coffeescript-gulp/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/aurium/coffeescript-gulp/downloads",
            "issues_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues{/number}",
            "pulls_url": "https://api.github.com/repos/aurium/coffeescript-gulp/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/aurium/coffeescript-gulp/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/aurium/coffeescript-gulp/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/aurium/coffeescript-gulp/labels{/name}",
            "releases_url": "https://api.github.com/repos/aurium/coffeescript-gulp/releases{/id}",
            "deployments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/deployments",
            "created_at": "2020-05-09T02:10:27Z",
            "updated_at": "2020-05-09T02:10:29Z",
            "pushed_at": "2020-05-09T02:17:41Z",
            "git_url": "git://github.com/aurium/coffeescript-gulp.git",
            "ssh_url": "git@github.com:aurium/coffeescript-gulp.git",
            "clone_url": "https://github.com/aurium/coffeescript-gulp.git",
            "svn_url": "https://github.com/aurium/coffeescript-gulp",
            "homepage": null,
            "size": 47,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": null,
            "has_issues": false,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 0,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 0,
            "license": null,
            "forks": 0,
            "open_issues": 0,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "base": {
          "label": "GeoffreyBooth:master",
          "ref": "master",
          "sha": "472fd8a8ea49ade5dee892463f618ecbb5dcdc65",
          "user": {
            "login": "GeoffreyBooth",
            "id": 456802,
            "node_id": "MDQ6VXNlcjQ1NjgwMg==",
            "avatar_url": "https://avatars2.githubusercontent.com/u/456802?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/GeoffreyBooth",
            "html_url": "https://github.com/GeoffreyBooth",
            "followers_url": "https://api.github.com/users/GeoffreyBooth/followers",
            "following_url": "https://api.github.com/users/GeoffreyBooth/following{/other_user}",
            "gists_url": "https://api.github.com/users/GeoffreyBooth/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/GeoffreyBooth/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/GeoffreyBooth/subscriptions",
            "organizations_url": "https://api.github.com/users/GeoffreyBooth/orgs",
            "repos_url": "https://api.github.com/users/GeoffreyBooth/repos",
            "events_url": "https://api.github.com/users/GeoffreyBooth/events{/privacy}",
            "received_events_url": "https://api.github.com/users/GeoffreyBooth/received_events",
            "type": "User",
            "site_admin": false
          },
          "repo": {
            "id": 64283764,
            "node_id": "MDEwOlJlcG9zaXRvcnk2NDI4Mzc2NA==",
            "name": "coffeescript-gulp",
            "full_name": "GeoffreyBooth/coffeescript-gulp",
            "private": false,
            "owner": {
              "login": "GeoffreyBooth",
              "id": 456802,
              "node_id": "MDQ6VXNlcjQ1NjgwMg==",
              "avatar_url": "https://avatars2.githubusercontent.com/u/456802?v=4",
              "gravatar_id": "",
              "url": "https://api.github.com/users/GeoffreyBooth",
              "html_url": "https://github.com/GeoffreyBooth",
              "followers_url": "https://api.github.com/users/GeoffreyBooth/followers",
              "following_url": "https://api.github.com/users/GeoffreyBooth/following{/other_user}",
              "gists_url": "https://api.github.com/users/GeoffreyBooth/gists{/gist_id}",
              "starred_url": "https://api.github.com/users/GeoffreyBooth/starred{/owner}{/repo}",
              "subscriptions_url": "https://api.github.com/users/GeoffreyBooth/subscriptions",
              "organizations_url": "https://api.github.com/users/GeoffreyBooth/orgs",
              "repos_url": "https://api.github.com/users/GeoffreyBooth/repos",
              "events_url": "https://api.github.com/users/GeoffreyBooth/events{/privacy}",
              "received_events_url": "https://api.github.com/users/GeoffreyBooth/received_events",
              "type": "User",
              "site_admin": false
            },
            "html_url": "https://github.com/GeoffreyBooth/coffeescript-gulp",
            "description": "Gulp task to watch CoffeeScript source files and recompile",
            "fork": false,
            "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp",
            "forks_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/forks",
            "keys_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/keys{/key_id}",
            "collaborators_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/collaborators{/collaborator}",
            "teams_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/teams",
            "hooks_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/hooks",
            "issue_events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/events{/number}",
            "events_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/events",
            "assignees_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/assignees{/user}",
            "branches_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/branches{/branch}",
            "tags_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/tags",
            "blobs_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/blobs{/sha}",
            "git_tags_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/tags{/sha}",
            "git_refs_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/refs{/sha}",
            "trees_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/trees{/sha}",
            "statuses_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/{sha}",
            "languages_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/languages",
            "stargazers_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/stargazers",
            "contributors_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/contributors",
            "subscribers_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/subscribers",
            "subscription_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/subscription",
            "commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/commits{/sha}",
            "git_commits_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/git/commits{/sha}",
            "comments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/comments{/number}",
            "issue_comment_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/comments{/number}",
            "contents_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/contents/{+path}",
            "compare_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/compare/{base}...{head}",
            "merges_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/merges",
            "archive_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/{archive_format}{/ref}",
            "downloads_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/downloads",
            "issues_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues{/number}",
            "pulls_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls{/number}",
            "milestones_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/milestones{/number}",
            "notifications_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/notifications{?since,all,participating}",
            "labels_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/labels{/name}",
            "releases_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/releases{/id}",
            "deployments_url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/deployments",
            "created_at": "2016-07-27T06:36:35Z",
            "updated_at": "2020-05-07T19:14:54Z",
            "pushed_at": "2020-05-07T19:14:51Z",
            "git_url": "git://github.com/GeoffreyBooth/coffeescript-gulp.git",
            "ssh_url": "git@github.com:GeoffreyBooth/coffeescript-gulp.git",
            "clone_url": "https://github.com/GeoffreyBooth/coffeescript-gulp.git",
            "svn_url": "https://github.com/GeoffreyBooth/coffeescript-gulp",
            "homepage": null,
            "size": 47,
            "stargazers_count": 0,
            "watchers_count": 0,
            "language": "CoffeeScript",
            "has_issues": true,
            "has_projects": true,
            "has_downloads": true,
            "has_wiki": true,
            "has_pages": false,
            "forks_count": 1,
            "mirror_url": null,
            "archived": false,
            "disabled": false,
            "open_issues_count": 1,
            "license": null,
            "forks": 1,
            "open_issues": 1,
            "watchers": 0,
            "default_branch": "master"
          }
        },
        "_links": {
          "self": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1"
          },
          "html": {
            "href": "https://github.com/GeoffreyBooth/coffeescript-gulp/pull/1"
          },
          "issue": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1"
          },
          "comments": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/issues/1/comments"
          },
          "review_comments": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/comments"
          },
          "review_comment": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/comments{/number}"
          },
          "commits": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/pulls/1/commits"
          },
          "statuses": {
            "href": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp/statuses/d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9"
          }
        },
        "author_association": "NONE",
        "merged": false,
        "mergeable": null,
        "rebaseable": null,
        "mergeable_state": "unknown",
        "merged_by": null,
        "comments": 0,
        "review_comments": 0,
        "maintainer_can_modify": true,
        "commits": 1,
        "additions": 1,
        "deletions": 0,
        "changed_files": 1
      }
    },
    "public": true,
    "created_at": "2020-05-09T02:18:22Z"
  },
  {
    "id": "12285556473",
    "type": "PushEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 262469253,
      "name": "aurium/coffeescript-gulp",
      "url": "https://api.github.com/repos/aurium/coffeescript-gulp"
    },
    "payload": {
      "push_id": 5044690399,
      "size": 1,
      "distinct_size": 1,
      "ref": "refs/heads/patch-1",
      "head": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
      "before": "472fd8a8ea49ade5dee892463f618ecbb5dcdc65",
      "commits": [
        {
          "sha": "d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9",
          "author": {
            "email": "30254+aurium@users.noreply.github.com",
            "name": "Aurélio A. Heckert"
          },
          "message": "Sync FS before build\n\nEnsure the file changes will be accessible by the builder process.",
          "distinct": true,
          "url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits/d4f7cf26ded94ca197876e50d14b4fe7ad0a21b9"
        }
      ]
    },
    "public": true,
    "created_at": "2020-05-09T02:17:42Z"
  },
  {
    "id": "12285532622",
    "type": "ForkEvent",
    "actor": {
      "id": 30254,
      "login": "aurium",
      "display_login": "aurium",
      "gravatar_id": "",
      "url": "https://api.github.com/users/aurium",
      "avatar_url": "https://avatars.githubusercontent.com/u/30254?"
    },
    "repo": {
      "id": 64283764,
      "name": "GeoffreyBooth/coffeescript-gulp",
      "url": "https://api.github.com/repos/GeoffreyBooth/coffeescript-gulp"
    },
    "payload": {
      "forkee": {
        "id": 262469253,
        "node_id": "MDEwOlJlcG9zaXRvcnkyNjI0NjkyNTM=",
        "name": "coffeescript-gulp",
        "full_name": "aurium/coffeescript-gulp",
        "private": false,
        "owner": {
          "login": "aurium",
          "id": 30254,
          "node_id": "MDQ6VXNlcjMwMjU0",
          "avatar_url": "https://avatars1.githubusercontent.com/u/30254?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/aurium",
          "html_url": "https://github.com/aurium",
          "followers_url": "https://api.github.com/users/aurium/followers",
          "following_url": "https://api.github.com/users/aurium/following{/other_user}",
          "gists_url": "https://api.github.com/users/aurium/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/aurium/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/aurium/subscriptions",
          "organizations_url": "https://api.github.com/users/aurium/orgs",
          "repos_url": "https://api.github.com/users/aurium/repos",
          "events_url": "https://api.github.com/users/aurium/events{/privacy}",
          "received_events_url": "https://api.github.com/users/aurium/received_events",
          "type": "User",
          "site_admin": false
        },
        "html_url": "https://github.com/aurium/coffeescript-gulp",
        "description": "Gulp task to watch CoffeeScript source files and recompile",
        "fork": true,
        "url": "https://api.github.com/repos/aurium/coffeescript-gulp",
        "forks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/forks",
        "keys_url": "https://api.github.com/repos/aurium/coffeescript-gulp/keys{/key_id}",
        "collaborators_url": "https://api.github.com/repos/aurium/coffeescript-gulp/collaborators{/collaborator}",
        "teams_url": "https://api.github.com/repos/aurium/coffeescript-gulp/teams",
        "hooks_url": "https://api.github.com/repos/aurium/coffeescript-gulp/hooks",
        "issue_events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/events{/number}",
        "events_url": "https://api.github.com/repos/aurium/coffeescript-gulp/events",
        "assignees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/assignees{/user}",
        "branches_url": "https://api.github.com/repos/aurium/coffeescript-gulp/branches{/branch}",
        "tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/tags",
        "blobs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/blobs{/sha}",
        "git_tags_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/tags{/sha}",
        "git_refs_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/refs{/sha}",
        "trees_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/trees{/sha}",
        "statuses_url": "https://api.github.com/repos/aurium/coffeescript-gulp/statuses/{sha}",
        "languages_url": "https://api.github.com/repos/aurium/coffeescript-gulp/languages",
        "stargazers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/stargazers",
        "contributors_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contributors",
        "subscribers_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscribers",
        "subscription_url": "https://api.github.com/repos/aurium/coffeescript-gulp/subscription",
        "commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/commits{/sha}",
        "git_commits_url": "https://api.github.com/repos/aurium/coffeescript-gulp/git/commits{/sha}",
        "comments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/comments{/number}",
        "issue_comment_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues/comments{/number}",
        "contents_url": "https://api.github.com/repos/aurium/coffeescript-gulp/contents/{+path}",
        "compare_url": "https://api.github.com/repos/aurium/coffeescript-gulp/compare/{base}...{head}",
        "merges_url": "https://api.github.com/repos/aurium/coffeescript-gulp/merges",
        "archive_url": "https://api.github.com/repos/aurium/coffeescript-gulp/{archive_format}{/ref}",
        "downloads_url": "https://api.github.com/repos/aurium/coffeescript-gulp/downloads",
        "issues_url": "https://api.github.com/repos/aurium/coffeescript-gulp/issues{/number}",
        "pulls_url": "https://api.github.com/repos/aurium/coffeescript-gulp/pulls{/number}",
        "milestones_url": "https://api.github.com/repos/aurium/coffeescript-gulp/milestones{/number}",
        "notifications_url": "https://api.github.com/repos/aurium/coffeescript-gulp/notifications{?since,all,participating}",
        "labels_url": "https://api.github.com/repos/aurium/coffeescript-gulp/labels{/name}",
        "releases_url": "https://api.github.com/repos/aurium/coffeescript-gulp/releases{/id}",
        "deployments_url": "https://api.github.com/repos/aurium/coffeescript-gulp/deployments",
        "created_at": "2020-05-09T02:10:27Z",
        "updated_at": "2020-05-07T19:14:54Z",
        "pushed_at": "2020-05-07T19:14:51Z",
        "git_url": "git://github.com/aurium/coffeescript-gulp.git",
        "ssh_url": "git@github.com:aurium/coffeescript-gulp.git",
        "clone_url": "https://github.com/aurium/coffeescript-gulp.git",
        "svn_url": "https://github.com/aurium/coffeescript-gulp",
        "homepage": null,
        "size": 47,
        "stargazers_count": 0,
        "watchers_count": 0,
        "language": null,
        "has_issues": false,
        "has_projects": true,
        "has_downloads": true,
        "has_wiki": true,
        "has_pages": false,
        "forks_count": 0,
        "mirror_url": null,
        "archived": false,
        "disabled": false,
        "open_issues_count": 0,
        "license": null,
        "forks": 0,
        "open_issues": 0,
        "watchers": 0,
        "default_branch": "master",
        "public": true
      }
    },
    "public": true,
    "created_at": "2020-05-09T02:10:28Z"
  }
];

displayActivityEvents(gitlabEvs, githubEvs);

