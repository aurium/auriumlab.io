import mkEl, { extendElement } from './mkel.js?t=%BUILD_DATE%';

"use strict";

const projectsEl = extendElement(document.getElementById('projects'));

const gitlabProjects = [
    '17819584',   // WAsm Heatmap
    '17232890',   // War 3D
    '14651162',   // planning-poker
    //'14524263', // Site Paru
    '10221895',   // Aurium’s Small Bot
    '7725873',    // run.js
    '7679448',    // JS13k 2018 - Pigeon
    '2253530',    // jsBoard
    //'1233050',  // AuriumBot
    //'607614',   // Remote Control
    //'218049',     // gEdit External Tools
    '216354',     // XPM Image
    '90879',      // inkex.js
    '90525',      // metajs
];
const gitlabGroups = [
    '252795',     // Exu
    '3947521',    // Chat Pieces
];
const githubRepos = [
    'aurium/dodos',
    'aurium/simland',
    'aurium/gravity',
];

function normalizeGitlabResp(response) {
    return response.json().then(data => {
        if (data.web_url.match(/groups/)) {
            return data;
        } else {
            return fetch(`https://gitlab.com/api/v4/projects/${data.id}/languages`)
            .then(response => response.json())
            .then(langs => {
                data.langs = langs;
                return data;
            })
        }
    });
}

function normalizeGithubResp(response) {
    return response.json().then(data => {
        data.web_url = data.html_url;
        return fetch(data.languages_url)
        .then(response => response.json())
        .then(langs => {
            data.langs = langs;
            return data;
        })
    });
}

gitlabProjects.forEach(id =>
    fetch(`https://gitlab.com/api/v4/projects/${id}?statistics=true`)
    .then(normalizeGitlabResp)
    .then(addProject)
    .catch(err => console.error('Fail while loading a project.', id, err))
)
gitlabGroups.forEach(id =>
    fetch(`https://gitlab.com/api/v4/groups/${id}?statistics=true`)
    .then(normalizeGitlabResp)
    .then(addProject)
    .catch(err => console.error('Fail while loading a group.', id, err))
)
githubRepos.forEach(id =>
    fetch(`https://api.github.com/repos/${id}`)
    .then(normalizeGithubResp)
    .then(addProject)
    .catch(err => console.error('Fail while loading a project.', id, err))
)

function addProject(project) {
    projectsEl.classList.remove('loading');
    let desc = project.description
        .replace(/(https?:\/\/([^\s]+))/g, `<a href="$1">$2</a>`);
    let projectEl = mkEl.li_project({
        parent: projectsEl,
        children: {
            a_pic: {href: project.web_url, children: {div:null}},
            _info: {children: {
                h3: {children: {a: {href: project.web_url, text: project.name}}},
                p_description: {html: desc},
                _footer: null
            }}
        }
    });
    let picLink = projectEl.querySelector('.pic');
    let picBox = projectEl.querySelector('.pic div');
    if (project.avatar_url) {
        picLink.classList.add('image');
        picBox.style.backgroundImage = `url(${project.avatar_url})`;
    } else {
        picLink.classList.add('letter');
        picBox.innerText = project.name[0].toUpperCase();
    }
    let footer = projectEl.querySelector('.footer');
    if (project.langs) addLangs(footer, project.langs);
    if (project.projects) addSubProject(footer, project.projects);
}

var langsMemo = [];

function addLangs(footerBox, langs) {
    let langsBox = footerBox.mkChild.ul_langs();
    Object.keys(langs).forEach(lang => {
        let index = langsMemo.indexOf(lang);
        if (index === -1) {
            index = langsMemo.length;
            langsMemo[index] = lang;
        }
        let langInfo = langs[lang];
        langsBox.mkChild.li({
            class: `lang-${lang.toLocaleLowerCase()} lang-${index}`,
            text: (lang==='JavaScript') ? 'JS' : lang
        })
    });
}

function addSubProject(footerBox, subProjects) {
    let subProjBox = footerBox.mkChild.ul_subProjects();
    subProjBox.mkChild.strong(subProjects.length + ' Sub Proj:');
    subProjects.forEach(subProj => {
        subProjBox.mkChild.li({
            title: subProj.description,
            text: subProj.name
        })
    });
    if (subProjBox.clientHeight > footerBox.clientHeight*1.2) {
        footerBox.classList.add('big');
    }
}

