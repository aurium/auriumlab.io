#!/bin/bash -e
shopt -s extglob

echo -e '\n>> Building Web Site...'

ROOT=$PWD
DIST=$ROOT/dist
test -e $DIST && rm -r $DIST
mkdir $DIST
cp -r assets/* $DIST/
cp -r src/*.{html,js} $DIST/
sass -c src/style/main.sass $DIST/main.css

meta_varname_re='pag_[_a-zA-Z0-9]+'
meta_var_re="^//-\s*$meta_varname_re\s*="

# TODO: move presentation to another repo. Can it be a inner-group?
DIST_IMPRESS=$DIST/presentation
PUG_TMPL_DIR=$ROOT/src/pug-tmpls
mkdir $DIST_IMPRESS
cd src/presentation
for dir in *; do
  echo ">> Building presentation $dir..."
  DIST_CUR="$DIST_IMPRESS/$dir"
  # Create the presentation dir
  mkdir "$DIST_CUR"
  cd $dir
  # Copy all non compilable files
  cp -rv !(*.pug|*.sass) "$DIST_CUR" 2>/dev/null || true
  # Parse main presentation file
  echo '>> Meta vars:'
  if egrep -q "$meta_var_re" index.pug; then
    egrep "$meta_var_re" index.pug | sed 's!//-\s*!• !'
  else
    echo '• No vars.'
  fi
  # Build data for presentation layout
  OBJ="$(
    egrep "$meta_var_re" index.pug |
    sed -r 's!//-\s*(.*)!global.\1,!'
    echo -n "{globals:["
    egrep "$meta_var_re" index.pug |
    sed -r "s!//-\s*($meta_varname_re).*!'\1',!"
    echo -n "'__']}"
  )"
  # Build the presentation page
  pug --pretty --basedir $PUG_TMPL_DIR --obj "$OBJ" < index.pug > "$DIST_CUR/index.html"
  # Build the style
  (
    echo "@use '$PUG_TMPL_DIR/presentation/base-style'"
    cat style.sass
  ) | sass --indented --stdin "$DIST_CUR/style.css"
done

# Update all %BUILD_DATE% references
NOW=$(date +%F_%T)
find $DIST -regex '.*\.\(html\|js\|css\)$' \
  -exec sed -ri "s/%BUILD_DATE%/$NOW/g"  '{}' ';'

echo # just a line break
